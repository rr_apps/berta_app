# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180919065207) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", id: :serial, force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_id", null: false
    t.string "resource_type", null: false
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "imports", id: :serial, force: :cascade do |t|
    t.string "imported_file"
    t.boolean "request_job"
    t.boolean "processed"
    t.integer "user_id"
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "processable"
    t.index ["imported_file"], name: "index_imports_on_imported_file"
    t.index ["processed"], name: "index_imports_on_processed"
    t.index ["request_job"], name: "index_imports_on_request_job"
  end

  create_table "ls_requests", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.datetime "date_joined_group"
    t.integer "years_service"
    t.integer "employee_number"
    t.string "preferred_name"
    t.string "last_name"
    t.string "location"
    t.integer "store_code"
    t.integer "store_payroll"
    t.string "store_name"
    t.string "payroll"
    t.string "language"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["import_id"], name: "index_ls_requests_on_import_id"
  end

  create_table "mm_adverts", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.integer "request_id"
    t.string "description"
    t.string "product"
    t.string "item_code"
    t.string "brand_short"
    t.string "store_type"
    t.string "region_short"
    t.integer "store_lang_region"
    t.boolean "store_specific"
    t.string "store_code"
    t.string "store_name"
    t.string "lang_full"
    t.string "lang_code"
    t.string "theme"
    t.string "theme_code"
    t.string "currency"
    t.decimal "price", precision: 8, scale: 2
    t.string "price_code"
    t.string "unit_size"
    t.string "start_date"
    t.string "end_date"
    t.string "dsf"
    t.boolean "pb_available"
    t.boolean "non_halaal"
    t.boolean "prev_spm_exists"
    t.integer "prev_spm_advert_id"
    t.boolean "new_spm_generated"
    t.string "spm_code"
    t.boolean "mp3_dsf_generated"
    t.boolean "sns_dsf_generated"
    t.boolean "sns_generated"
    t.boolean "request_processed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["description"], name: "index_mm_adverts_on_description"
    t.index ["dsf"], name: "index_mm_adverts_on_dsf"
    t.index ["end_date"], name: "index_mm_adverts_on_end_date"
    t.index ["import_id"], name: "index_mm_adverts_on_import_id"
    t.index ["item_code"], name: "index_mm_adverts_on_item_code"
    t.index ["mp3_dsf_generated"], name: "index_mm_adverts_on_mp3_dsf_generated"
    t.index ["new_spm_generated"], name: "index_mm_adverts_on_new_spm_generated"
    t.index ["non_halaal"], name: "index_mm_adverts_on_non_halaal"
    t.index ["pb_available"], name: "index_mm_adverts_on_pb_available"
    t.index ["price"], name: "index_mm_adverts_on_price"
    t.index ["price_code"], name: "index_mm_adverts_on_price_code"
    t.index ["product"], name: "index_mm_adverts_on_product"
    t.index ["request_id"], name: "index_mm_adverts_on_request_id"
    t.index ["request_processed"], name: "index_mm_adverts_on_request_processed"
    t.index ["sns_dsf_generated"], name: "index_mm_adverts_on_sns_dsf_generated"
    t.index ["sns_generated"], name: "index_mm_adverts_on_sns_generated"
    t.index ["spm_code"], name: "index_mm_adverts_on_spm_code"
    t.index ["start_date"], name: "index_mm_adverts_on_start_date"
  end

  create_table "mm_brand_shorts", id: :serial, force: :cascade do |t|
    t.string "brand"
    t.string "brand_short"
    t.string "store_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mm_currencies", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.string "rr_voice_code"
    t.string "lang_code"
    t.string "curr_code"
    t.string "curr_type"
    t.string "file_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["curr_code"], name: "index_mm_currencies_on_curr_code"
    t.index ["curr_type"], name: "index_mm_currencies_on_curr_type"
    t.index ["import_id"], name: "index_mm_currencies_on_import_id"
    t.index ["lang_code"], name: "index_mm_currencies_on_lang_code"
    t.index ["rr_voice_code"], name: "index_mm_currencies_on_rr_voice_code"
  end

  create_table "mm_excel_read_regions", id: :serial, force: :cascade do |t|
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mm_non_halaal_keywords", id: :serial, force: :cascade do |t|
    t.string "keyword"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mm_pre_recorded_body_files", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.string "rr_voice_code"
    t.integer "item_code"
    t.string "description"
    t.string "unit"
    t.string "lang_code"
    t.string "file_name"
    t.string "job_number"
    t.integer "price_gaps"
    t.text "script"
    t.boolean "non_halaal", default: false
    t.boolean "non_kosher", default: false
    t.string "tagging_sheet_name"
    t.string "app_note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["description"], name: "index_mm_pre_recorded_body_files_on_description"
    t.index ["import_id"], name: "index_mm_pre_recorded_body_files_on_import_id"
    t.index ["item_code"], name: "index_mm_pre_recorded_body_files_on_item_code"
    t.index ["non_halaal"], name: "index_mm_pre_recorded_body_files_on_non_halaal"
    t.index ["non_kosher"], name: "index_mm_pre_recorded_body_files_on_non_kosher"
    t.index ["rr_voice_code"], name: "index_mm_pre_recorded_body_files_on_rr_voice_code"
    t.index ["unit"], name: "index_mm_pre_recorded_body_files_on_unit"
  end

  create_table "mm_price_value_files", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.string "rr_voice_code"
    t.string "lang_code"
    t.string "value_type"
    t.integer "value"
    t.string "unit_size"
    t.string "file_name"
    t.string "app_note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["file_name"], name: "index_mm_price_value_files_on_file_name"
    t.index ["import_id"], name: "index_mm_price_value_files_on_import_id"
    t.index ["lang_code"], name: "index_mm_price_value_files_on_lang_code"
    t.index ["rr_voice_code"], name: "index_mm_price_value_files_on_rr_voice_code"
    t.index ["unit_size"], name: "index_mm_price_value_files_on_unit_size"
    t.index ["value"], name: "index_mm_price_value_files_on_value"
    t.index ["value_type"], name: "index_mm_price_value_files_on_value_type"
  end

  create_table "mm_region_lists", id: :serial, force: :cascade do |t|
    t.string "region_long"
    t.string "region_short"
    t.string "brand_types"
    t.string "store_lang_region"
    t.string "lang"
    t.string "currency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mm_reports", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mm_request_jobs", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.string "job_name"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean "job_completed"
    t.boolean "job_pending"
    t.boolean "job_failed"
    t.boolean "job_queued"
    t.boolean "request_accepted"
    t.boolean "advert_requests_accepted"
    t.boolean "job_requests_accepted"
    t.boolean "job_zip_created"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["end_date"], name: "index_mm_request_jobs_on_end_date"
    t.index ["import_id"], name: "index_mm_request_jobs_on_import_id"
    t.index ["job_completed"], name: "index_mm_request_jobs_on_job_completed"
    t.index ["start_date"], name: "index_mm_request_jobs_on_start_date"
  end

  create_table "mm_requests", id: :serial, force: :cascade do |t|
    t.integer "import_id"
    t.integer "item_code"
    t.string "item_descr"
    t.string "brand_short"
    t.string "store_type"
    t.string "region_short"
    t.integer "store_lang_region"
    t.string "store_code"
    t.string "store_name"
    t.datetime "from"
    t.datetime "to"
    t.decimal "price", precision: 8, scale: 2
    t.string "unit"
    t.text "extra_1"
    t.boolean "request_ok"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["item_code"], name: "index_mm_requests_on_item_code"
    t.index ["item_descr"], name: "index_mm_requests_on_item_descr"
  end

  create_table "mm_store_codes", id: :serial, force: :cascade do |t|
    t.integer "lngstore"
    t.integer "branch_code", null: false
    t.string "store_name", null: false
    t.integer "store_type"
    t.string "store_type_long", null: false
    t.string "store_type_abbr", null: false
    t.string "store_division", null: false
    t.integer "store_lang_region", null: false
    t.boolean "halaal_store", default: false
    t.boolean "kosher_store", default: false
    t.boolean "include_afr", default: false
    t.boolean "exclude_store", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_code"], name: "index_mm_store_codes_on_branch_code"
    t.index ["exclude_store"], name: "index_mm_store_codes_on_exclude_store"
    t.index ["halaal_store"], name: "index_mm_store_codes_on_halaal_store"
    t.index ["include_afr"], name: "index_mm_store_codes_on_include_afr"
    t.index ["kosher_store"], name: "index_mm_store_codes_on_kosher_store"
    t.index ["store_division"], name: "index_mm_store_codes_on_store_division"
    t.index ["store_name"], name: "index_mm_store_codes_on_store_name"
    t.index ["store_type"], name: "index_mm_store_codes_on_store_type"
    t.index ["store_type_abbr"], name: "index_mm_store_codes_on_store_type_abbr"
    t.index ["store_type_long"], name: "index_mm_store_codes_on_store_type_long"
  end

  create_table "mm_store_languages", id: :serial, force: :cascade do |t|
    t.integer "lngstore"
    t.boolean "english"
    t.boolean "afrikaans"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["afrikaans"], name: "index_mm_store_languages_on_afrikaans"
    t.index ["english"], name: "index_mm_store_languages_on_english"
    t.index ["lngstore"], name: "index_mm_store_languages_on_lngstore"
  end

  create_table "music_files", id: :serial, force: :cascade do |t|
    t.string "file_name"
    t.string "title"
    t.integer "import_id"
    t.string "app_note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["file_name"], name: "index_music_files_on_file_name"
    t.index ["import_id"], name: "index_music_files_on_import_id"
    t.index ["title"], name: "index_music_files_on_title"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.boolean "approved", default: false, null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "voice_artists", id: :serial, force: :cascade do |t|
    t.integer "rr_voice_code"
    t.string "voice_fname"
    t.string "voice_lname"
    t.string "lang_code"
    t.string "language"
    t.decimal "pad_1", precision: 8, scale: 2
    t.decimal "pad_2", precision: 8, scale: 2
    t.decimal "pad_3", precision: 8, scale: 2
    t.decimal "curr_pad", precision: 8, scale: 2
    t.decimal "unit_pad", precision: 8, scale: 2
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
