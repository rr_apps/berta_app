# frozen_string_literal: true
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

REGIONS = [
  'Western Cape Checkers Division',
  'Western Cape Shoprite Division',
  'Eastern Cape',
  'Natal Division',
  'Gauteng Shoprites',
  'Gauteng Checkers& Hypers',
  'Gauteng  & Northern Division Shoprite stores',
  'Free State/OFS',
  'Free state/Northern Cape',
  'Lesotho',
  'Namibia',
  'Swaziland',
  'Swailand',
  'Kwazulu Natal Division',
  'Northern Cape /OFS',
  'Gauteng  & Great North Shoprites',
  'Gauteng Checkers & Hypers',
  'Western Cape'
].freeze
puts "adding Regions "
REGIONS.each do |reg|
  MmExcelReadRegion.where(region: reg).first_or_create(region: reg)
end

# our hack to ensure that we cover possible requests for SR.
BRANDS = [
  { 'brand' => 'Checkers & Hypers', 'brand_short' => 'CH CHHY', 'store_type' => '1, 9' },
  { 'brand' => 'Shoprite', 'brand_short' => 'SR', 'store_type' => '2, 5' },
  { 'brand' => 'Ethnic', 'brand_short' => 'ECP', 'store_type' => '5' },
  { 'brand' => 'Checkers/Shoprite', 'brand_short' => 'CH SR', 'store_type' => '1, 5' },
  { 'brand' => 'Lesotho', 'brand_short' => 'LESOTHO SR', 'store_type' => '5' },
  { 'brand' => 'Checkers', 'brand_short' => 'CH', 'store_type' => '1' },
  { 'brand' => 'Hypers Only', 'brand_short' => 'CHHY', 'store_type' => '9' },
  { 'brand' => 'Hypers', 'brand_short' => 'CHHY', 'store_type' => '9' },
  { 'brand' => 'Shoprite /Ethnic', 'brand_short' => 'SR ECP', 'store_type' => '5' },
  { 'brand' => 'Shoprite / Checkers & Hypers', 'brand_short' => 'SR CH CHHY', 'store_type' => '5, 1, 9' }
].freeze

# As per the RR CC db
# 1;"Checkers";"CH";"C";"store0001"
# 2;"Super Store";"SS";"U";"store0002"
# 3;"OK";"OK";"O";"store0003"
# 4;"Village Market";"VM";"V";"store0004"
# 5;"Shoprite";"SR";"S";"store0005"
# 6;"Distribution Centre";"DC";"D";"store0006"
# 7;"Hyperama";"HY";"H";"store0007"
# 9;"Checkers Hyper";"CA";"A";"store0008"
# 10;"OK Foods";"OF";"K";"store0009"
# 11;"Non Foods";"NF";"N";"store0010"
# 12;"Usave";"US";"E";"store0011"
# 13;"MediRite";"MR";"M";"store0012"
# 14;"LiquorShop";"LS";"L";"store0013"
# 15;"Shoprite Offices";"SRO";"W";"store0017"
# 18;"MediRite Plus";"MRP";"R";"store0020"
# 16;"LiquorShop Checkers";"LC";"X";"store0018"
# 17;"LiquorShop Shoprite";"LS";"Y";"store0019"
# 19;"Super Usave";"SU";"P";"store0014"
# 20;"Hungry Lion";"HL";"Q";"store0015"
# 21;"ZaPOP Offices";"ZO";"T";"store0016"
# 22;"Shoprite Mini";"SM";"B";"store0021"
# 23;"Coffee Shop";"CS";"Z";"store0022"
puts "adding Brands"
BRANDS.each do |brand|
  MmBrandShort.where(brand: brand['brand']).first_or_create(brand:       brand['brand'],
                                                            brand_short: brand['brand_short'],
                                                            store_type:  brand['store_type'])
end

# Eastern Cape Division : 1
# Western Cape Division : 2
# Natal Division : 3
# Northern Division : 4
# Northern Cape and O.F.S Division : 5
# Gauteng Division : 6
# Namibia Division : 8
# Mozambique Division : 16
# Lesotho Divsion : 17
# Botswana Division : 18
# Swaziland Division : 19
# Checkers GN Division : 20
# Malawi : 21
# Angola : 26
# DRC (Democratic Republic of Congo) : 28

REGIONS_LIST = [
  { 'region' => 'Eastern Cape', 'reg' => 'EC', 'brand_types' => 'any', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '1' },
  { 'region' => 'Free State/OFS', 'reg' => 'OFS', 'brand_types' => 'any', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '5' },
  { 'region' => 'Free state/Northern Cape', 'reg' => 'OFS', 'brand_types' => 'any', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '5' },
  { 'region' => 'Gauteng  & Great North Shoprites', 'reg' => 'GN', 'brand_types' => 'SR', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '6' },
  { 'region' => 'Gauteng  & Northern Division Shoprite stores', 'reg' => 'GN', 'brand_types' => 'SR', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '6' },
  { 'region' => 'Gauteng Checkers & Hypers',  'reg' => 'GN', 'brand_types' => 'CH', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '20' },
  { 'region' => 'Gauteng Checkers& Hypers', 'reg' => 'GN', 'brand_types' => 'CH', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '20' },
  { 'region' => 'Gauteng Shoprites', 'reg' => 'GN', 'brand_types' => 'SR', 'lang' => 'Eng & Afr',  'currency' => 'zar', 'slr' => '6' },
  { 'region' => 'Kwazulu Natal Division', 'reg' => 'KZN', 'brand_types' => 'any', 'lang' => 'Eng', 'currency' => 'zar', 'slr' => '3' },
  { 'region' => 'Lesotho',  'reg' => 'LESOTHO', 'brand_types' => 'any', 'lang' => 'Eng',  'currency' => 'lsl',  'slr' => '17' },
  { 'region' => 'Namibia',  'reg' => 'NAMIBIA', 'brand_types' => 'any', 'lang' => 'Eng',  'currency' => 'nad',  'slr' => '8' },
  { 'region' => 'Natal Division',  'reg' => 'KZN', 'brand_types' => 'any', 'lang' => 'Eng', 'currency' => 'zar', 'slr' => '3' },
  { 'region' => 'Northern Cape /OFS',  'reg' => 'OFS', 'brand_types' => 'any', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '5' },
  { 'region' => 'Swailand', 'reg' => 'SWAZILAND', 'brand_types' => 'any', 'lang' => 'Eng', 'currency' => 'szl', 'slr' => '19' },
  { 'region' => 'Swaziland', 'reg' => 'SWAZILAND', 'brand_types' => 'any', 'lang' => 'Eng', 'currency' => 'szl', 'slr' => '19' },
  { 'region' => 'Western Cape', 'reg' => 'WC', 'brand_types' => 'any', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '2' },
  { 'region' => 'Western Cape Checkers Division',  'reg' => 'WC', 'brand_types' => 'any', 'lang' => 'Eng & Afr',  'currency' => 'zar',  'slr' => '2' },
  { 'region' => 'Western Cape Shoprite Division',  'reg' => 'WC', 'brand_types' => 'any', 'lang' => 'Eng & Afr',  'currency' => 'zar',  'slr' => '2' },
  { 'region' => 'All', 'reg' => 'WC OFS GN', 'brand_types' => 'any', 'lang' => 'Eng & Afr', 'currency' => 'zar', 'slr' => '2,5,6,20' }
].freeze

puts "adding Regions_list"
REGIONS_LIST.each do |list|
  MmRegionList.where(region_long: list['region']).first_or_create(region_long:       list['region'],
                                                                  region_short:      list['reg'],
                                                                  brand_types:       list['brand_types'],
                                                                  lang:              list['lang'],
                                                                  store_lang_region: list['slr'],
                                                                  currency: list['currency'])
end

VOICES_ARTISTS = [
  {
    rr_voice_code: 56,
    voice_fname: 'anton',
    voice_lname: 'brink',
    lang_code: 'L01',
    language: 'afr',
    pad_1: 0.09,
    pad_2: 0.058,
    pad_3: 0.5,
    curr_pad: 0.02,
    unit_pad: 0.06
  },
  {
    rr_voice_code: 56,
    voice_fname: 'anton',
    voice_lname: 'brink',
    lang_code: 'L02',
    language: 'eng',
    pad_1: 0.1,
    pad_2: 0.0,
    pad_3: 0.5,
    curr_pad: 0.02,
    unit_pad: 0.06
  },
  {
    rr_voice_code: 212,
    voice_fname: 'gina',
    voice_lname: '',
    lang_code: 'L01',
    language: 'afr',
    pad_1: 0.15,
    pad_2: 0.058,
    pad_3: 0.5,
    curr_pad: 0.05,
    unit_pad: 0.07
  },
  {
    rr_voice_code: 212,
    voice_fname: 'gina',
    voice_lname: '',
    lang_code: 'L02',
    language: 'eng',
    pad_1: 0.15,
    pad_2: 0.058,
    pad_3: 0.5,
    curr_pad: 0.02,
    unit_pad: 0.07
  },
  {
    rr_voice_code: 270,
    voice_fname: 'jordan',
    voice_lname: 'gray',
    lang_code: 'L02',
    language: 'eng',
    pad_1: 0.25,
    pad_2: 0.27,
    pad_3: 0.5,
    curr_pad: 0.01,
    unit_pad: 0.05
  }
].freeze
puts "adding Voice Artists"
VOICES_ARTISTS.each do |name|
  voice_artist = VoiceArtist.where('rr_voice_code = ? and lang_code = ?', name[:rr_voice_code], name[:lang_code]).first_or_create(
    rr_voice_code: name[:rr_voice_code],
    voice_fname: name[:voice_fname],
    voice_lname: name[:voice_lname],
    lang_code: name[:lang_code],
    language: name[:language],
    pad_1: name[:pad_1],
    pad_2: name[:pad_2],
    pad_3: name[:pad_3],
    curr_pad: name[:curr_pad],
    unit_pad: name[:unit_pad]
  )
  voice_artist.save!
end

NON_HALAAL_KEYWORDS = [
  'pork',
  'gammon',
  'meat pack',
  'rib'
].freeze

NON_HALAAL_KEYWORDS.each do |keyword|
  MmNonHalaalKeyword.create!(keyword: keyword)
end



USER_SEED = [
  { 'admin@radioretail.co.za' => 'admin123' },
  { 'drteren@gmail.com'         => 'david123' },
  { 'isabeau@zapop.com'         => 'isabeau123' },
  { 'nic@radioretail.co.za'     => 'nic12345' },
  { 'annedi@radioretail.co.za'  => 'annedi123' },
  { 'martilize@zapop.com'       => 'martilize123' },
  { 'philip@radioretail.co.za'  => 'philip123' }
].freeze

USER_SEED.each do |user|
  email, password = user.first
  User.where(email: email).
    first_or_create!(email: email,
                     password: password,
                     password_confirmation: password,
                     approved: true)
  AdminUser.where(email: email).
    first_or_create!(email: email,
                     password: password,
                     password_confirmation: password)
end


# StoreCodeRequester.update_stores

MmStoreCodeRequester.update_stores

puts "Store Code Seeder"
# MmStoreCodeSeeder.seed_table if MmStoreCode.all.empty?

HALAAL_STORES = [
    { '064434' => 'SR ACTONVILLE' },
    { '042737' => 'SR LENASIA' },
    { '000814' => 'SR MAYFAIR' },
    { '046260' => 'SR ROCKLANDS' },
    { '030716' => 'SR OTTERY' },
    { '046189' => 'SR BISHOP LAVIS' },
    { '046202' => 'SR HANOVER PARK' },
    { '041935' => 'SR GATESVILLE' },
    { '046197' => 'SR ATHLONE' },
    { '002230' => 'SR CHATSWORTH' },
    { '039273' => 'SR VERULAM HIGHWAY' }
].freeze

puts "adding Hallal stores"
HALAAL_STORES.each do |store|
  MmStoreCode.where(branch_code: store.keys[0]).
      update(halaal_store: true)
end

KOSHER_STORES = [
    { '2549' => 'CH BALFOUR PARK' }
].freeze

KOSHER_STORES.each do |store|
  MmStoreCode.where(branch_code: store.keys[0]).
      update(kosher_store: true)
end
