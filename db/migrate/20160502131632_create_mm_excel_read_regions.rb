class CreateMmExcelReadRegions < ActiveRecord::Migration
  def change
    create_table :mm_excel_read_regions do |t|
      t.string :region

      t.timestamps null: false
    end
  end
end
