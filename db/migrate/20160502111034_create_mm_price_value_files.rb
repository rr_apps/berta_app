class CreateMmPriceValueFiles < ActiveRecord::Migration
  def change
    create_table :mm_price_value_files do |t|
      t.integer :import_id, index: true
      t.string :rr_voice_code, index: true
      t.string :lang_code, index: true
      t.string :value_type, index: true
      t.integer :value, index: true
      t.string :unit_size, index: true
      t.string :file_name, index: true
      t.string :app_note

      t.timestamps null: false
    end
  end
end
