class CreateMusicFiles < ActiveRecord::Migration
  def change
    create_table :music_files do |t|
      t.string :file_name, index: true
      t.string :title, index: true
      t.integer :import_id, index: true
      t.string :app_note

      t.timestamps null: false
    end
  end
end
