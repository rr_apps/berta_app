class CreateMmRegionLists < ActiveRecord::Migration
  def change
    create_table :mm_region_lists do |t|
      t.string :region_long
      t.string :region_short
      t.string :brand_types
      t.string :store_lang_region
      t.string :lang
      t.string :currency
      t.timestamps null: false
    end
  end
end
