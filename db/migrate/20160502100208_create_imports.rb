class CreateImports < ActiveRecord::Migration
  def change
    create_table :imports do |t|
      t.string :imported_file, index: true
      t.boolean :request_job, index: true
      t.boolean :processed, index: true
      t.integer :user_id
      t.string :note

      t.timestamps null: false
    end
  end
end
