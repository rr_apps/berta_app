class CreateMmCurrencies < ActiveRecord::Migration[5.0]
  def change
    create_table :mm_currencies do |t|
      t.integer :import_id, index: true
      t.string :rr_voice_code, index: true
      t.string :lang_code, index: true
      t.string :curr_code, index: true
      t.string :curr_type, index: true
      t.string :file_name
      t.timestamps
    end
  end
end
