class CreateLsRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :ls_requests do |t|
      t.integer :import_id, index: true
      t.datetime :date_joined_group
      t.integer :years_service
      t.integer :employee_number
      t.string :preferred_name
      t.string :last_name
      t.string :location
      t.integer :store_code
      t.integer :store_payroll
      t.string :store_name
      t.string :payroll
      t.string :language
      t.timestamps
    end
  end
end
