class CreateMmBrandShorts < ActiveRecord::Migration
  def change
    create_table :mm_brand_shorts do |t|
      t.string :brand
      t.string :brand_short
      t.string :store_type
      t.timestamps null: false
    end
  end
end
