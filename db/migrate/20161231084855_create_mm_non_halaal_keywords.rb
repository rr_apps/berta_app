class CreateMmNonHalaalKeywords < ActiveRecord::Migration[5.0]
  def change
    create_table :mm_non_halaal_keywords do |t|
      t.string :keyword
      t.timestamps
    end
  end
end
