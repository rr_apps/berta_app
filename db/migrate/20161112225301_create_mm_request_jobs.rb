class CreateMmRequestJobs < ActiveRecord::Migration[5.0]
  def change
    create_table :mm_request_jobs do |t|
      t.integer :import_id, index: true
      t.string :job_name
      t.datetime :start_date, index: true
      t.datetime :end_date, index: true
      t.boolean :job_completed, index: true
      t.boolean :job_pending
      t.boolean :job_failed
      t.boolean :job_queued
      t.boolean :request_accepted
      t.boolean :advert_requests_accepted
      t.boolean :job_requests_accepted
      t.boolean :job_zip_created
      t.integer :user_id
      t.timestamps
    end
  end
end
