class CreateMmPreRecordedBodyFiles < ActiveRecord::Migration
  def change
    create_table :mm_pre_recorded_body_files do |t|
      t.integer :import_id, index: true
      t.string :rr_voice_code, index: true
      t.integer :item_code, index: true
      t.string :description, index: true
      t.string :unit, index: true
      t.string :lang_code
      t.string :file_name
      t.string :job_number
      t.integer :price_gaps
      t.text :script
      t.boolean :non_halaal, default: false, index: true
      t.boolean :non_kosher, default: false, index: true
      t.string :tagging_sheet_name
      t.string :app_note

      t.timestamps null: false
    end
  end
end
