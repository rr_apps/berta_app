class AddProcecessableToImports < ActiveRecord::Migration[5.1]
  def change
    add_column :imports, :processable, :boolean
  end
end
