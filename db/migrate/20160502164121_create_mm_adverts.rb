class CreateMmAdverts < ActiveRecord::Migration
  def change
    create_table :mm_adverts do |t|
      t.integer :import_id, index: true
      t.integer :request_id, index: true
      t.string :description, index: true
      t.string :product, index: true
      t.string :item_code, index: true
      t.string :brand_short
      t.string :store_type
      t.string :region_short
      t.integer :store_lang_region
      t.boolean :store_specific
      t.string :store_code
      t.string :store_name
      t.string :lang_full
      t.string :lang_code
      t.string :theme
      t.string :theme_code
      t.string :currency
      t.decimal :price, :precision => 8, :scale => 2, index: true
      t.string :price_code, index: true
      t.string :unit_size
      t.string :start_date, index: true
      t.string :end_date, index: true
      t.string :dsf, index: true
      t.boolean :pb_available, index: true
      t.boolean :non_halaal, index: true
      t.boolean :prev_spm_exists
      t.integer :prev_spm_advert_id
      t.boolean :new_spm_generated, index: true
      t.string  :spm_code, index: true
      t.boolean :mp3_dsf_generated, index: true
      t.boolean :sns_dsf_generated, index: true
      t.boolean :sns_generated, index: true
      t.boolean :request_processed, index: true
      t.timestamps null: false
    end
  end
end
