class CreateMmStoreLanguages < ActiveRecord::Migration[5.0]
  def change
    create_table :mm_store_languages do |t|
      t.integer :lngstore, index: true
      t.boolean :english, index: true
      t.boolean :afrikaans, index: true
      t.timestamps
    end
  end
end
