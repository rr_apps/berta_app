class CreateMmStoreCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :mm_store_codes do |t|
      t.integer :lngstore
      t.integer :branch_code, null: false, index: true
      t.string :store_name, null: false, index: true
      t.integer :store_type, nul: false, index: true
      t.string :store_type_long, null: false, index: true
      t.string :store_type_abbr, null: false, index: true
      t.string :store_division, null: false, index: true
      t.integer :store_lang_region, null: false
      t.boolean :halaal_store, default: false, index: true
      t.boolean :kosher_store, default: false, index: true
      t.boolean :include_afr, default: false, index: true
      t.boolean :exclude_store, default: false, index: true
      t.timestamps
    end
  end
end
