class CreateVoiceArtists < ActiveRecord::Migration
  def change
    create_table :voice_artists do |t|
      t.integer :rr_voice_code
      t.string :voice_fname
      t.string :voice_lname
      t.string :lang_code
      t.string :language
      t.decimal :pad_1, :precision => 8, :scale => 2
      t.decimal :pad_2, :precision => 8, :scale => 2
      t.decimal :pad_3, :precision => 8, :scale => 2
      t.decimal :curr_pad, :precision => 8, :scale => 2
      t.decimal :unit_pad, :precision => 8, :scale => 2
      t.timestamps null: false
    end
  end
end
