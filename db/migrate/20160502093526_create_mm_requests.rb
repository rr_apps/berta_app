class CreateMmRequests < ActiveRecord::Migration
  def change
    create_table :mm_requests do |t|
      t.integer :import_id
      t.integer :item_code, index: true
      t.string :item_descr, index: true
      t.string :brand_short
      t.string :store_type
      t.string :region_short
      t.integer :store_lang_region
      t.string :store_code
      t.string :store_name
      t.datetime :from
      t.datetime :to
      t.decimal :price, precision: 8, scale: 2
      t.string :unit
      t.text :extra_1
      t.boolean :request_ok
      t.timestamps null: false
    end
  end
end
