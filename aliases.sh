#!/usr/bin/env bash

# Various bash script for use in production
# add the following to .bash_profile
## source ~/meatmarket/aliases


export APP_PATH="$HOME/meatmarket"

alias psql_start='sudo service postgresql start'
alias psql_start='sudo service postgresql stop'

# Web server
alias nginx_restart='sudo service nginx restart'
alias nginx_start='sudo service nginx start'
alias nginx_rstop='sudo service nginx stop'

# App server
alias puma_stop='sudo stop puma-manager'
alias puma_start='sudo start puma-manager'
alias puma_restart='sudo restart puma-manager'

# Log viewers
# Rails logs
alias log_rails_dev='tail -n 1000 $APP_PATH/log/development.log'
alias log_rails_prod='tail -n 1000 $APP_PATH/log/production.log'

# Puma logs
alias log_puma_err='tail -n 1000 $APP_PATH/shared/log/puma.stderr.log'
alias log_puma_out='tail -n 1000 $APP_PATH/shared/log/puma.stdout.log'

# Nginx logs
alias log_nginx_err='tails -n 1000 /home/devops/logs/nginx/error.log'
alias log_nginx_err='tails -n 1000 /home/devops/logs/nginx/access.log'

# Rails Stuff

alias bundle_for_production='cd $APP_PATH && bundle install --without development test'

alias db_create='cd $APP_PATH && RAILS_ENV=production rake db:create'
alias db_migrate='cd $APP_PATH && RAILS_ENV=production rake db:migrate'
alias db_seed='cd $APP_PATH && RAILS_ENV=production rake db:seed'
alias assets_precompile='cd $APP_PATH && RAILS_ENV=production rake assets:precompile'

alias rails_console='RAILS_ENV=production rails c'
