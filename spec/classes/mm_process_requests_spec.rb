# frozen_string_literal: true
require 'rails_helper'

RSpec.describe ExcelRowTypes, type: :module do

  describe 'set' do

    context "given an empty row" do
      it "returns the word: 'ignore'" do
        expect(subject.set([])).to eq('ignore')
      end
    end

    context "given an empty string" do
      it "returns the word: 'ignore'" do
        expect(subject.set(["  "])).to eq('ignore')
      end
    end

    context "given the title" do
      it "returns the word: 'ignore'" do
        expect(subject.set(["Shoprite Checkers Meat Markets"]
        )).to eq('ignore')
      end
    end

    context "given a header" do
      it "returns the word: 'ignore'" do
        expect(subject.set(["Item No",
                            "Item Description ",
                            "Brand",
                            "From",
                            "To",
                            "Price"])).to eq('ignore')
      end
    end

    context "given specials date range" do
      it "returns the word: 'ignore'" do
        expect(subject.set(["Specials for the Period: ",
                            nil,
                            "Mon, 01 May 2017",
                            "to", "Sun, 07 May 2017"])).to eq('ignore')
      end
    end

    context "given an exclude Halaal store message" do
      it "returns the word: 'exclude_halaal'" do
        expect(subject.set(["EXCLUDING HALAAL STORES H/PARK/ATHLONE/GATESVILLE/OTTERY"])).to eq('ignore')
      end
    end

    context "given an exclude Kosher store message" do
      it "returns the word: 'ignore'" do
        expect(subject.set(["EXCLUDING Kosher STORES H/PARK/Sea Point/"])).to eq('ignore')
      end
    end

    context "given a store code" do
      it "returns the word: 'store_specific'" do
        expect(subject.set(["RR6352"])).to eq('store_specific')
      end
    end

    context "given multiple store codes" do
      it "returns the word: 'multi_store_specific'" do
        expect(subject.set(["RR6352/RR666/RR763"])).to eq('multi_store_specific')
      end
    end

    context "given an item_no as float" do
      it "returns the word: 'item_request'" do
        expect(subject.set([1978708.0,
                            "Lamb Liver",
                            "Shoprite",
                            "Mon, 01 May 2017",
                            "Sun, 07 May 2017",
                            26.99,
                            "/kg"])).to eq('item_request')
      end
    end

    context "given an item_no as integer" do
      it "returns the word: 'item_request'" do
        expect(subject.set([1978708,
                            "Lamb Liver",
                            "Shoprite",
                            "Mon, 01 May 2017",
                            "Sun, 07 May 2017",
                            26.99,
                            "/kg"])).to eq('item_request')
      end
    end

    context "given multiple item_nos as slash '/' seperated string" do
      it "returns the word: 'multiple_item_requests'" do
        expect(subject.set(["5392380/586865",
                            "Lamb Liver",
                            "Shoprite",
                            "Mon, 01 May 2017",
                            "Sun, 07 May 2017",
                            26.99,
                            "/kg"])).to eq('multiple_item_requests')
      end
    end

    REGION_TEST = [["Western Cape Checkers Division"],
                   ["Western Cape Shoprite   Division", " "],
                   ["Northern Cape/ OFS"],
                   ["Gauteng Checkers  & Hyper"],
                   ["Namibia "],
                   ["Gauteng Shoprite & Northern Shoprite", nil, nil, nil, nil, nil, nil, nil],
                   ["Gauteng Checkers  & Hyper"]
    ]
    REGIONS_LIST = ["Gauteng Shoprite & Northern Shoprite",
                    "Gauteng Checkers & Hyper",
                    "Western Cape Checkers Division",
                    "Western Cape Shoprite Division",
                    "Northern Cape/OFS",
                    "Namibia"]

    context "given a region tht exist in the database" do
      REGION_TEST.each do |region|
        it "returns the word: 'region_name'" do
          allow(subject).to receive(:region_names).and_return(REGIONS_LIST)
          expect(subject.set(region)).to eq('region')
        end
      end
    end

    context 'given item rows with missing values (less than seven colums)' do
      it "returns the word: 'error_unknown'" do
        expect(subject.set([1978825, " Beef Brisket", "Shoprite", "Wed, 22 Aug 2018", "Fri, 24 Aug 2018", 74.99])).to eq('error_unknown')
      end
    end

    UNKOWNS = [["Specific Stores : Makhaza / Khayelitsha 023/ Station Plaza / Russel Street"],
               ["Famous for Catalogue:  Western Cape, Eastern Cape, Kwazulu Natal, Northern Cape, Gauteng (Checker & Hypers)"],
               ["Parow Park Only 3910"],
               ["Checkers & Hypers (Western Cape/Eastern Cape/Kwazulu Natal/Northern Cape/Gauteng)"],
               ["New Store Opening : Mica Mall"]
    ]

    context "given an unknown" do
      UNKOWNS.each do |unknown|
        it "returns the word: 'error_unknown'" do
          expect(subject.set(unknown)).to eq('error_unknown')
        end
      end
    end
  end
end
