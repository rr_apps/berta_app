# frozen_string_literal: true
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
# Full-stack web application framework. (http://www.rubyonrails.org)
gem 'rails', '5.1.5'


gem 'turbolinks'

gem 'puma'

# gem 'sidekiq'

# gem 'carrierwave', '~> 0.5'
gem 'carrierwave_backgrounder', git: 'https://github.com/lardawge/carrierwave_backgrounder.git'

# Use postgresql as the database for Active Record
# Pg is the Ruby interface to the {PostgreSQL RDBMS}[http://www.postgresql.org/] (https://bitbucket.org/ged/ruby-pg)
gem 'pg'

# Ruby file upload library (https://github.com/carrierwaveuploader/carrierwave)
gem 'carrierwave'

gem 'autoprefixer-rails'
gem 'bootstrap-sass'
gem 'bootswatch-rails'
gem 'font-awesome-rails'
# Forms made easy! (https://github.com/plataformatec/simple_form)
gem 'simple_form'

# Embed the V8 JavaScript interpreter into Ruby (http://github.com/cowboyd/therubyracer)
# gem 'therubyracer'
gem 'mini_racer'

# Use Uglifier as compressor for JavaScript assets
# Ruby wrapper for UglifyJS JavaScript compressor (http://github.com/lautis/uglifier)
gem 'uglifier'

# Use SCSS for stylesheets
# Sass adapter for the Rails asset pipeline. (https://github.com/rails/sass-rails)
gem 'sass-rails'
# Use CoffeeScript for .coffee assets and views
# CoffeeScript adapter for the Rails asset pipeline. (https://github.com/rails/coffee-rails)
gem 'coffee-rails'

# Use jquery as the JavaScript library
# Use jQuery with Rails 4+ (http://rubygems.org/gems/jquery-rails)
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# Turbolinks makes following links in your web application faster (use with Rails Asset Pipeline) (https://github.com/rails/turbolinks/)
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# Create JSON structures via a Builder-style DSL (https://github.com/rails/jbuilder)
gem 'jbuilder'
# bundle exec rake doc:rails generates the API under doc/api.
# rdoc html with javascript search index. (http://github.com/voloko/sdoc)
# gem 'sdoc', '~> 0.4.0', group: :doc

# Roo can access the contents of various spreadsheet files. (http://github.com/roo-rb/roo)
gem 'roo'

# Roo::Excel can access the contents of classic xls files.
gem 'roo-xls'

# Read low-level informations and manipulate tags on mp3 files. (http://github.com/toy/mp3info)
gem 'mp3info'

# Flexible authentication solution for Rails with Warden (https://github.com/plataformatec/devise)
gem 'devise'

# administration framework
# The administration framework for Ruby on Rails. (http://activeadmin.info)
gem 'activeadmin', github: 'activeadmin'
# gem 'inherited_resources', github: 'activeadmin/inherited_resources'

# Series of Actions with an emphasis on simplicity. Ideal for multi-step processes
# gem 'light-service'

gem 'rollbar'
gem 'oj'

gem 'sidekiq'

group :development, :test do

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # Ruby 2.0 fast debugger - base + CLI (http://github.com/deivid-rodriguez/byebug)
  gem 'byebug'

  # Better error page for Rack apps
  # Better error page for Rails and other Rack apps (https://github.com/charliesome/better_errors)
  gem 'better_errors'
  #  handle events on file system modifications
  # Guard keeps an eye on your file modifications (http://guardgem.org)

  gem 'rails-erd'

  gem 'binding_of_caller'

  gem 'guard'
  # automatically reload your browser when 'view' files are modified
  # Guard plugin for livereload (https://rubygems.org/gems/guard-livereload)
  gem 'guard-livereload', require: false
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # Rails application preloader (http://github.com/rails/spring)
  gem 'spring'

  # A mini view framework for console/irb that's easy to use, even while under its influence. (http://tagaholic.me/hirb/)
  gem 'hirb'

  # Use Pry as your rails console (https://github.com/rweng/pry-rails)
  gem 'pry-rails'

  # Fast debugging with Pry. (https://github.com/deivid-rodriguez/pry-byebug)
  gem 'pry-byebug'

  # Simple execution navigation for Pry. (https://github.com/nixme/pry-nav)
  gem 'pry-nav'

  gem 'rspec-rails'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  # A debugging tool for your Ruby on Rails applications. (https://github.com/rails/web-console)
  gem 'web-console'
end
