# BERTA - Best Ever Radio Transmission App #

This application is used to process the weekly Meat Market requests for in-store radio specials from Shoprite/Checkers as well as the monthly LongService requests.

### Features:
* Bulk import of required audio assets
* Reads Excel spreadsheets as received from client with 



### Requirements ###
* Ruby ver 2.3.4 
* Rails 5.0.0 +
* Natural Numbers - Audio Processor https://bitbucket.org/rr_apps/nn_audio_processor
* SoX - [Sound eXchange audio processor](http://sox.sourceforge.net/)
* LAME - [MPEG Audio Layer III (MP3) encoder](http://lame.sourceforge.net/)

### Usage ###

View the [Wiki](https://bitbucket.org/rr_apps/berta_app/wiki/Home)


### Logging & Temp Directory ###

The app will look for the logging file and temp directory paths in the `AUDIO_PROC_LOG` & `AUDIO_PROC_TEMP` environmental variables.
Should one not be set it will default writing the log in the root dir of the app.

To set the environmental variables:

    export NNAP_TEMP_DIR="$HOME/factory/logs/audio_processor.log"
    export  NNAP_TEMP_DIR="/tmp/audio_temp"
    
