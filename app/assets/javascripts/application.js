//= require jquery3
//= require jquery_ujs
//= require turbolinks

//= require bootstrap-sprockets
//= require active_admin


$(document).ready(function() {
    $('.has-tooltip').tooltip();
});

$(document).ready(function() {
    $('.has-tooltip').tooltip();
    $('.has-popover').popover({
        trigger: 'hover'
    });
});
