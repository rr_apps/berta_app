
# frozen_string_literal: true
class MmAdvertsController < ApplicationController
  def index
    @requests = Import.where(id: MmAdvert.distinct.pluck(:import_id))
  end

  def create
    StoreCodeRequesterJob.perform_now
    MmRequestJob.where(import_id: params[:id]).update(request_accepted: true)
    @requests = MmRequest.where(import_id: params[:id])
    import_id = @requests.first.import_id
    ProcessAdvertJob.perform_now(@requests)
    @adverts = MmAdvert.where(import_id: import_id)
    @import  =  MmRequestJob.where(import_id: params[:id]).last
    render 'show'
  end

  def show
    @adverts = MmAdvert.where(import_id: params[:id])
    @import  =  MmRequestJob.where(import_id: params[:id]).last
  end
end
