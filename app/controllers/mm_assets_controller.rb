# frozen_string_literal: true
class MmAssetsController < ApplicationController

  def index
    @voice_artists = VoiceArtist.all
    @music = MusicFile.all
    @voices_with_price = MmPriceValueFile.distinct.pluck(:rr_voice_code)
    @pb_bodies = MmPreRecordedBodyFile.all
    @price_value_files = MmPriceValueFile.all
  end

  def pb_bodies
    @pb_bodies = MmPreRecordedBodyFile.all.order('LOWER(description)')
  end

  def required
    @pbs_required_all     = MmAdvert.where(pb_available: false)
    @pb_required_distinct = MmAdvert.where(pb_available: false).select('distinct on (item_code, lang_code) * ')
    @pbs_all              = MmPreRecordedBodyFile.all
  end
end
