# frozen_string_literal: true
class ImportsController < ApplicationController
  include ExcelsHelper
  include CleanupHelper

  def index
    @imports = Import.all
  end

  def process_zip_file
    import      = Import.find(params[:id])
    file        = import.imported_file.file.file
    id          = import.id
    @file_name  = File.basename(file)
    ImporterJob.perform_now(file, id)
    @imports = Import.all
    redirect_to imports_path, notice: "The import #{import.imported_file.filename} has been processed."
  end

  def new
    @import = Import.new
  end

  def create
    @import = Import.new(import_params)
    @import.user_id = current_user.id
    @import.processable = true
    if @import.save
      redirect_to imports_path, notice: "The import #{@import.imported_file.filename} has been uploaded."
    else
      render 'new'
    end
  end

  def destroy
    @import = Import.find(params[:id])
    @import.destroy
    @requests = MmRequest.where(import_id: @import.id)
    @requests.destroy_all
    @adverts = MmAdvert.where(import_id: @import.id)
    @adverts.destroy_all
    @job_requests = MmRequestJob.where(import_id: @import.id).destroy_all
    redirect_to imports_path, notice: "The import #{@import.imported_file.filename} has been deleted."
  end

  def destroy_all
    clear_all_imports
    redirect_to imports_path, notice: 'ALL DESTROYED'
  end

  private

  def import_params
    params.require(:import).permit(:imported_file, :user_id)
  end
end
