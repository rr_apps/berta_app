class MmRequestsController < ApplicationController
  def index
    @requests = Import.where(id: MmRequest.distinct.pluck(:import_id))
  end
  def create
    import = Import.find(params[:id])
    file = import.imported_file.file.file
    id = import.id

    job_name = Import.where(id: params[:id])[0].imported_file.file.file.split('/').last.gsub('.xlsx', '').gsub('.xls', '').gsub('Copy_of_', '')

    MmRequestJob.where(import_id: params[:id]).first_or_create(import_id: params[:id], job_name: job_name)

    MmRequestValidator.new(file, id)
    import = Import.find(params[:id])

    if import.processable
      MmProcessRequests.new(file, id)
      @file_name = File.basename(file)
      @requests = MmRequest.where(import_id: id)
      @import = MmRequestJob.where(import_id: params[:id]).last
      import.request_job = true
      import.save
      render 'show'
    else
      redirect_to imports_path, alert: "There are issues with the request sheet. #{import.note}"
    end
  end

  def show
    @requests = MmRequest.where(import_id: params[:id])
    @import = MmRequestJob.where(import_id: params[:id]).last
  end
end
