# frozen_string_literal: true
class MmRequestJobsController < ApplicationController

  def index
    @request_jobs = MmRequestJob.all
    @requests_all = MmRequest.all
    @adverts_all = MmAdvert.all
  end

  def download_job
    job = MmRequestJob.where(id: params[:format]).first
    date = Time.now.strftime('%Y-%m-%d')
    filename = %(#{date}_#{job.id}_#{job.job_name}.zip)
    zip_file = File.join(SPM, job.id.to_s + '.zip' )
    send_file(
        zip_file,
        filename: filename,
        type: "application/zip",
        stream: false
    )
  end

  def create
    MmRequestJob.where(import_id: params[:id]).update(job_requests_accepted: true)
    redirect_to mm_request_jobs_path
  end

  def new
    job_id = params[:format]
    @request_job = MmRequestJob.where(id: job_id)
    import_id =  @request_job[0].import_id

    # AdvertsGeneratorJob.perform_later(adverts, job_id, import_id, current_user.id)
    AdvertsGeneratorJob.perform_now(job_id, import_id, current_user.id)
    flash[:warn] = 'Job completed. The adverts have been generated.'
    redirect_to mm_request_job_path(import_id)
  end

  def show
    @request_job = MmRequestJob.where(import_id: params[:id])
    @adverts = MmAdvert.where(import_id: params[:id]).order('request_id')
    @import  =  MmRequestJob.where(import_id: params[:id]).last
  end

end
