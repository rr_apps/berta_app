class LsRequestsController < ApplicationController

  def index
  end

  def new
  end

  def create
    import      = Import.find(params[:format])
    file        = import.imported_file.file.file
    id          = import.id

    # job_name = Import.where(id: params[:id])[0].imported_file.file.file.split('/').last.delete('.xlsx').delete('.xls')
    # MmRequestJob.where(import_id: params[:id]).first_or_create(import_id: params[:id], job_name: job_name)

    # MmRequestProcess.perform_now(file, id)
    @file_name  = File.basename(file)
    @requests   = MmRequest.where(import_id: id)
    render 'show'
  end

  def show
  end
end
