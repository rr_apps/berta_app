# frozen_string_literal: true
class MmReportsController < ApplicationController

  def halaal_stores
    @halaal_stores = MmStoreCode.has_halaal_stores
  end

  def non_halaal_products
    @non_halaal_products = MmPreRecordedBodyFile.where(non_halaal: true)
  end

end
