# frozen_string_literal: true
require 'simple_spy'
module ExcelsHelper

  def clean_array(array)
    return 'not found' if array.nil?
    array = array.delete('[').delete(']') .delete('"')
    res = array.split(',').count == 1
    array = remove_trailing_comma(array) if res
    array
  end

  def status_region(region)
    @res = 'Fail'
    @error_count += 1
    return 'danger' if region.nil?
    return 'danger' if region == 'not found'
    return 'danger' if region.split(',').count > 1
    @error_count -= 1
    @res = 'OK'
  end

  def button_type(value)
    if value
      content_tag(:span, 'Ok', class: 'btn  label-success btn-xs')
    else
      content_tag(:span, 'Fail', class: 'btn  label-danger btn-xs')
    end
  end

  def remove_trailing_comma(str)
    str.nil? ? nil : str.chomp(',')
  end

  def update_note(error_count)
    note = 'Job can be run.' if error_count.zero?
    note = "Failed with #{error_count} " + 'error'.pluralize(error_count) if error_count.positive?
    Import.find(@requests.first.import_id).update(note: note)
  end

end
