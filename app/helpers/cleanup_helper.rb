
# frozen_string_literal: true
module CleanupHelper

  def clear_all_imports
    Import.where(request_job: true).destroy_all
    # PriceValueFile.destroy_all
    # PreRecordedBodyFile.destroy_all
    # MusicFile.destroy_all
    Import.where('imported_file ilike ?', '%xls%').destroy_all
    MmRequest.destroy_all
    MmAdvert.destroy_all
    MmRequestJob.destroy_all

    # clear_dir(MUSIC)
    # clear_dir(PBS)
    # clear_dir(VALS)
    # clear_dir(TAGGING)
    clear_dir(JOBS)
    clear_dir(LOGS)
    clear_dir(SPM_ZIP_PATH)
  end

  def clear_dir(dir)
    FileUtils.rm_rf(Dir.glob(File.join(dir, '*')))
  end
end
