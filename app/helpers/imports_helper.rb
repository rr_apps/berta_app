# frozen_string_literal: true
module ImportsHelper
  def process_by_type(import)
    return content_tag(:span, 'Processed') if processed_state(import)
    return content_tag(:span, 'Unprocessable') unless import.processable

    return button_to('Request Job Ready',
                     mm_request_jobs_path,
                     method: :post,
                     class: 'btn btn-success btn-xs',
                     data: { disable_with: "Processing. Please wait..." }) if job_state(import)

    type = File.extname(import.imported_file.file.file)
    return button_to('Process XLS',
                     mm_requests_path(import.id),
                     params: { id: import.id },
                     method: :post,
                     class: 'btn btn-warning btn-xs',
                     data: { disable_with: "Processing. Please wait..." })  if type == '.xls' || type == '.xlsx'

    return button_to('Process Zip',
                     process_zip_file_import_path(import),
                     method: :put,
                     class: 'btn btn-warning btn-xs',
                     data: { disable_with: "Processing. Please wait..." })  if type == '.zip'
  end

  def import_row_color(import)
    if import.request_job.eql?(true)
      return 'warning' if import.note.nil?
      return 'warning' if import.note.empty?
      return 'success' if import.note.include?('Job completed')
    else
      return 'info' if import.processed.eql?(true)
    end
    'warning'
  end

  def trash_or_locked(import)
    return fa_icon "lock 2x" if import.processed.eql?(true)
    link_to '<i class="fa fa-trash-o fa-2x" aria-hidden="true"></i>'.html_safe, import_path(import.id), method: :delete
  end

  def date_format(time)
    time.localtime.strftime('%d-%m-%y %H:%M')
  end
end
