# frozen_string_literal: true
module MmAssetsHelper

  def voice_name(voice)
    VoiceArtist.where(rr_voice_code: voice)[0].name
  end

  def voice_full_name(voice_code)

    vo = VoiceArtist.where(rr_voice_code: voice_code)[0]
    %(#{vo.voice_fname.capitalize} #{vo.voice_lname.capitalize})
  end

end
