# frozen_string_literal: true
module MmReportsHelper
  def generate_rr_code(code)
    "RR#{sprintf '%06d', code}"
  end

end
