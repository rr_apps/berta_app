# frozen_string_literal: true
module MmRequestJobsHelper

  def process_by_state(job)
    return 'Job Not Accepted!' unless job.job_requests_accepted
    state = job.job_completed ? true : false
    # return button_to('Run Job', { controller: :mm_request_jobs, action: :new, id: job.id }, class: 'btn btn-success btn-xs') unless state

    if job.job_zip_created && state.eql?(true)
      return button_to('Download', mm_request_jobs_download_job_path(job),
                       method: :get, class: 'btn btn-success btn-xs')
    end

    return button_to('Run Job', new_mm_request_job_path(job),
                     method: :get, class: 'btn btn-success btn-xs',
                     data: { disable_with: "Processing. Please wait..." }) unless state
    button_to('Failed', '#',
              class: 'btn btn-danger btn-xs')
  end

end
