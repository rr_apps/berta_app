# frozen_string_literal: true
module ApplicationHelper
  def processed_state(import)
    return true if import.processed == true
    false
  end

  def job_state(import)
    return true if import.request_job == true
    false
  end


  def time_format(time)
    time.localtime.strftime('%d %b %Y %H:%M:%S')
  end
end
