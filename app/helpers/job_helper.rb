# frozen_string_literal: true
module JobHelper

  def row_state(advert)
    return 'danger' if advert.eql?(false)
    return 'success' if advert.eql?(true)
    return 'danger' unless advert.pb_available
    return 'success' if advert.request_processed
    'info'
  end

  def status_halaal(advert)
    return 'exclamation-triangle' unless  advert.pb_available.eql?(true)
    return 'thumbs-o-down' if advert.non_halaal
    return 'thumbs-o-up'
  end

  def pb_available(advert)
    return 'thumbs-o-up' if advert.pb_available.eql?(true)
    return 'exclamation-triangle'
  end

  def request_processed(advert)
    return 'recycle' if advert.prev_spm_exists.eql?(true)
    return 'thumbs-o-up' if advert.new_spm_generated.eql?(true)
    return 'exclamation-triangle' unless  advert.pb_available.eql?(true)
  end
end
