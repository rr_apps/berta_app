# frozen_string_literal: true
module MmAdvertsHelper

  def pbs_not_available(adverts)
    adverts.where(pb_available: false).to_a.uniq(&:description).size
  end




end
