# frozen_string_literal: true
class ImporterJob < ApplicationJob
  queue_as :default

  def perform(file, id)
    FileImporter.new(file, id)
  end

end
