# frozen_string_literal: true
class StoreCodeRequesterJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    # Do something later
    MmStoreCodeRequester.update_stores
  end
end
