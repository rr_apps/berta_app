# frozen_string_literal: true
class AdvertsGeneratorJob < ApplicationJob
  queue_as :default

  def perform(job_id, import_id, current_user_id)
    MmAdvertsGenerator.run_process(job_id, import_id, current_user_id)
  end
end
