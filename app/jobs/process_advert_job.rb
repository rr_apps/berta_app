# frozen_string_literal: true
class ProcessAdvertJob < ApplicationJob
  queue_as :default

  def perform(requests)
    MmProcessAdverts.new.requests_to_adverts(requests)
  end
end
