# frozen_string_literal: true
class MmRequestProcess < ApplicationJob
  queue_as :default

  def perform(file, id)
    MmProcessRequests.new(file, id)
  end
end
