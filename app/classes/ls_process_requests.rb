require 'roo-xls'

class LsProcessRequests

  def initialize(ls_request_excel)
    @excel_rows = Roo::Spreadsheet.open(ls_request_excel)
  end

  def process
    ls_requests = []
    num = 0
    @excel_rows.each do |row|
      ls_request = {}

      if has_header?(row)
        num =+ 1
        next
      end

      ls_request[:no]                 = num += 1
      ls_request[:date_joined_group]  = date_joined_group(row) # make this a date type
      ls_request[:years_service]      = row[1].to_i
      ls_request[:employee_number]    = row[2].to_i
      ls_request[:preferred_name]     = row[3].strip
      ls_request[:last_name]          = row[4].strip
      ls_request[:location]           = row[5].strip
      ls_request[:store_code]         = row[6].to_i
      ls_request[:store_payroll]      = row[7].to_i
      ls_request[:store_name]         = row[8].strip
      ls_request[:payroll]            = row[9].strip
      ls_request[:language]           = row[10].strip
      ls_requests << ls_request
    end
    ls_requests
  end

  def has_header?(row)
    row[0].include?('Date Joined') || row[0].include?('Store Num')
  end

  def date_joined_group(row)
     p a_num?(row[0])
    day, month, year = row[0].strip.split('.')
    %(#{year}/#{month}/#{day})
  end

  def a_num?(str)
    !!Integer(str)
  rescue ArgumentError, TypeError
    false
  end
end

if __FILE__ == $PROGRAM_NAME
  ls_request_excel = '/Users/davidteren/Projects/RadioRetail/BERTA_Project/03_LongServiceProject/01_Requests_From_Shoprite/Radio Retail September 2016.xls'
  LsProcessRequests.new(ls_request_excel).process
end
