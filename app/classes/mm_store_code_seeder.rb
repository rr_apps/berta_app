
class MmStoreCodeSeeder

  STORE_SEED = File.join(Rails.root, 'db', 'store_list_seeds.csv')

  def self.seed_table
    csv_text = File.read(STORE_SEED)
    csv = CSV.parse(csv_text,  :headers => true)
    csv.each do |row|
      MmStoreCode.create!(row.to_h)
    end
  end

end
