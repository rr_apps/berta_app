class MmRequestValidator
  attr_reader  :client_excel, :id


  def initialize(file, id)
    @file = file
    @id = id
    @client_excel = Roo::Spreadsheet.open(file)
   return request_sheet_pre_check?
  end

  def request_sheet_pre_check?
    return true if check_for_excel_issues.empty?
    Import.find(id).update(note: "The following rows cannot be processed: #{check_for_excel_issues}",
                                   processable: false)
    false
  end

  def check_for_excel_issues
    row_no = 0
    rows_with_issues = []
    client_excel.each do |row|
      row_no += 1
      rows_with_issues << row_no if ExcelRowTypes.set(row).include?('error_unknown')
    end
    p rows_with_issues
  end

end