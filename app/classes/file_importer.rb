# frozen_string_literal: true
class FileImporter
  include Utils

  # idenitify the zip type: PB's, Vals or Music
  def initialize(file, id)
    Utils.create_dirs
    @log = Logger.new(File.join(LOGS, 'pb_import.log'))
    @log.info('running')
    file_basename = File.basename(file)
    process_music(file, id) if file_basename.include?('music_tagging')
    process_val(file, id) if file_basename.include?('val_tagging')
    process_pb(file, id) if file_basename.include?('pb_tagging')
  end

  def process_music(zip_file, id)
    unzip(zip_file, JOBS)
    file_basename = File.basename(zip_file, '.zip')
    tag_file_location = File.join(JOBS, file_basename)
    files = file_list(tag_file_location)
    file_count = 0
    files.each do |file|
      file_path = File.join(JOBS, file_basename, file)
      mp3_title = mp3_info(file_path).tag.title
      file_name = random_hex + '_' + file

      next if MusicFile.exists?(title: mp3_title)
      file_count += 1
      music = MusicFile.where('title = ?', mp3_title).
          first_or_create(file_name: file_name,
                          title: mp3_title,
                          import_id: id)
      music.save

      rename_and_move_file(MUSIC, file_name, file_path)
    end
    note = "Added #{file_count} music files"
    update_import_status(id, note)
    clear_job_dir
  end

  def process_val(zip_file, id)
    ## TODO: Tag VO_name , VO-Code & value to the MP3's

    # Unzip the imported file
    unzip(zip_file, JOBS)

    # get a list of the directories in unipped dir
    dirs = file_list(File.join(JOBS, 'val_tagging'))

    # setup a counter
    import_status_count = 0
    # Iterate through the dirs
    dirs.each do |dir|
      rr_voice_code, value_type, language = dir.split('_')

      # Process the files, determine the type and build the file name
      # [voice code][language code][type][size]
      # eg: 202L02US1KG 202L01US300G

      value_type = set_value_type(dir, value_type)
      # Set the language code
      lang_code = 'L01' if language.to_s.downcase.include?('af') || language.to_s.downcase.include?('l01')
      lang_code = 'L02' if language.to_s.downcase.include?('en') || language.to_s.downcase.include?('l02')

      # Ge the list of files in the dir
      files = file_list(File.join(JOBS, 'val_tagging', dir))

      # Iterate through the list of files
      files.each do |file|
        file_name = rr_voice_code + value_type + lang_code + file
        value = file.gsub('.mp3', '')
        if value_type.include?('UNITSIZE')
          next if MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and unit_size = ?',
                                         rr_voice_code, lang_code,
                                         value_type, value).exists?
          import_status_count += 1
          price_value = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and unit_size = ?',
                                               rr_voice_code, lang_code, value_type, value).
              first_or_create(import_id: id,
                              rr_voice_code: rr_voice_code,
                              lang_code: lang_code,
                              value_type: value_type,
                              unit_size: value,
                              file_name: file_name)
          price_value.save
        end

        if value_type.include?('VAL')

          next if MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and value = ?',
                                         rr_voice_code, lang_code,
                                         value_type, value).exists?

          import_status_count += 1
          price_value = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and value = ?',
                                               rr_voice_code, lang_code, value_type, value).
              first_or_create(import_id: id,
                              rr_voice_code: rr_voice_code,
                              lang_code: lang_code,
                              value_type: value_type,
                              value: value,
                              file_name: file_name)
          price_value.save
        end

        if value_type.include?('CURR')
          next if MmCurrency.where('rr_voice_code = ? and lang_code = ? and curr_code = ? and curr_type = ?',
                                   rr_voice_code, lang_code,
                                   value.upcase, value_type).exists?

          import_status_count += 1
          currency = MmCurrency.where('rr_voice_code = ? and lang_code = ? and curr_code = ? and curr_type = ?',
                                      rr_voice_code, lang_code, value.upcase, value_type).
              first_or_create(import_id: id,
                              rr_voice_code: rr_voice_code,
                              lang_code: lang_code,
                              curr_code: value.upcase,
                              curr_type: value_type,
                              file_name: file_name)
          currency.save
        end

        file_path = File.join(JOBS, 'val_tagging', dir, file)

        rename_and_move_file(VALS, file_name, file_path)
      end
    end
    note = "Added #{import_status_count} price files"
    update_import_status(id, note)
    clear_job_dir
  end

  def set_value_type(dir, value_type)
    value_type = 'UNITSIZE' if dir.include?('unit')
    value_type = 'VALA1'    if value_type == 'vala'    || value_type == 'vala1'
    value_type = 'VALB1'    if value_type == 'valb'    || value_type == 'valb1'
    value_type = 'VALA2'    if value_type == 'vala2'
    value_type = 'VALB2'    if value_type == 'valb2'
    value_type = 'CURRA1'   if value_type == 'curra1'  || value_type == 'curra'
    value_type = 'CURRB1'   if value_type == 'currb1'  || value_type == 'currb'
    value_type = 'CURRA2'   if value_type == 'curra2'
    value_type = 'CURRB2'   if value_type == 'currb2'
    value_type = 'CURRE1'   if value_type == 'curre1' || value_type == 'curre'
    value_type = 'CURRE2'   if value_type == 'curre2'
    value_type
  end

  def process_pb(zip_file, id)

    # TODO: Tag VO_name , VO-Code & desciption. script to the MP3's
    unzip(zip_file, JOBS)
    tag_filename        = random_hex + '_' + 'tag_sheet.xlsx'
    tag_file_excel      = File.join(TAGGING, tag_filename)
    file_basename       = File.basename(zip_file, '.zip')
    tag_file_location   = File.join(JOBS, file_basename)
    tagging_excel = File.join(tag_file_location, 'tag_sheet.xlsx')
    while ! File.exists?(File.join(JOBS, file_basename)) do
      unzip(zip_file, File.join(JOBS, file_basename))
    end
    FileUtils.mv(tagging_excel, tag_file_excel)
    files = file_list(tag_file_location)

    non_halaal_keywords = MmNonHalaalKeyword.pluck(:keyword)

    tag_list = Roo::Spreadsheet.open(tag_file_excel)

    file_count = 0
    tag_list.each do |row|

      next if row[0].nil?
      next unless row[0].include?('Pre-recorded Body')

      rr_voice_code = row[2].to_s
      item_code = row[3]
      description = row[4].to_s.strip.chomp
      unit = row[5]
      lang_code = lang_code(row[6])
      mp3_file = row[7]
      job_number = row[8]
      price_gaps = row[9]
      script = row[10]

      file_path = File.join(JOBS, file_basename, mp3_file)
      media_present = File.exist?(file_path)
      @log.warn('Media not found: ' + mp3_file) unless media_present
      next unless media_present

      # file_name = random_hex + '_' + mp3_filex
      file_name = rr_voice_code + lang_code + '_' + mp3_file

      pb_item_exists = MmPreRecordedBodyFile.where('rr_voice_code = ? and item_code = ? and unit = ? and lang_code = ?',
                                                   rr_voice_code, item_code, unit, lang_code).exists?

      if pb_item_exists

        pb_item = MmPreRecordedBodyFile.where('rr_voice_code = ? and item_code = ? and unit = ? and lang_code = ?',
                                              rr_voice_code, item_code, unit, lang_code)
        @log.error('PB exists: ' + row.to_s)
        @log.error('PB exists: ' + pb_item.inspect)
      end

      next if pb_item_exists

      non_halaal = check_for_non_halaal?(non_halaal_keywords, description) ? true : false

      file_count += 1
      new_item = MmPreRecordedBodyFile.where('rr_voice_code = ? and item_code = ? and unit = ? and lang_code = ?',
                                             rr_voice_code, item_code, unit, lang_code).
          first_or_create(import_id: id,
                          rr_voice_code: rr_voice_code,
                          item_code: item_code,
                          description: description,
                          unit: unit,
                          lang_code: lang_code,
                          file_name: file_name,
                          job_number:  job_number,
                          price_gaps: price_gaps,
                          script: script,
                          tagging_sheet_name: tag_filename,
                          non_halaal: non_halaal,
                          non_kosher: non_halaal,
                          app_note: 'tagged')

      rename_and_move_file(PBS, file_name, file_path)
      @log.info(%(New item added: #{new_item.inspect}))
    end

    note = "Added #{file_count} pre-recorded bodies"
    update_import_status(id, note)
    clear_job_dir
  end

  def rename_and_move_file(asset_type, file_name, file_path)
    FileUtils.mkdir_p(File.dirname(file_path))
    FileUtils.mv(file_path, File.join(asset_type, file_name))
  end

  def mp3_info(file)
    Mp3Info.open(file) do |mp3info|
      mp3info
    end
  end

  def file_list(dir)
    Dir.entries(dir) - %w(. .. .DS_Store)
  end

  def random_hex
    SecureRandom.hex(6)
  end

  def clear_job_dir
    FileUtils.rm_rf(Dir.glob(File.join(JOBS, '*')))
  end

  def update_import_status(id, note)
    Import.find(id).update(processed: true, note: note)
  end

  def lang_code(lang)
    return 'L02' if lang.downcase.include?('eng')
    return 'L01' if lang.downcase.include?('afr')
    'L02'
  end

  def check_for_non_halaal?(non_halaal_keywords, description)
    non_halaal_keywords.each do |keyword|
      return true if description.downcase.include?(keyword.downcase)
    end
    false
  end

  def unzip(source, dest)
    Zip::File.open(source) do |zip_file|
      zip_file.each do |f|
        f_path = File.join(dest, f.name)
        FileUtils.mkdir_p(File.dirname(f_path))
        zip_file.extract(f, f_path) unless File.exist?(f_path)
      end
    end
  end
end
