# frozen_string_literal: true

module ConstantsHelper
  APP_RT       = File.join(Rails.root)
  FACTORY      = File.join(APP_RT, '..', 'mm_factory')
  JOBS         = File.join(FACTORY, 'jobs')
  DSF          = File.join(JOBS, 'dsf')
  LOGS         = File.join(FACTORY, 'logs')
  SNS          = File.join(JOBS, 'sns')
  SNS_DSF      = File.join(JOBS, 'sns_dsf')
  SPM          = File.join(JOBS, 'spm')
  OUTGOING     = File.join(JOBS, 'outgoing')
  SPM_ZIP_PATH = File.join(APP_RT, 'public', 'zip')
  SPM_JSON     = File.join(JOBS, 'spm_json')
  MUSIC        = File.join(FACTORY, 'assets', 'music')
  PBS          = File.join(FACTORY, 'assets', 'pb')
  VALS         = File.join(FACTORY, 'assets', 'val')
  TAGGING      = File.join(FACTORY, 'assets', 'tagging')
  TEMP         = File.join(JOBS, 'temp')
  TAG_FILE     = File.join(FACTORY, 'jobs')
  RUBY         = `which ruby`.chomp
  NNAP         = File.join(APP_RT, '..', 'nnap', 'nnap.rb')
  DISTRO       = File.join(FACTORY, 'test_distro', 'some_path')
end

include ConstantsHelper
