# frozen_string_literal: true
class MmStoreCodeRequester

  # SELECT * FROM tlkstore_type ORDER BY strstorecode
  # 1;"Checkers";"CH";"C";"store0001"
  # 2;"Super Store";"SS";"U";"store0002"
  # 3;"OK";"OK";"O";"store0003"
  # 4;"Village Market";"VM";"V";"store0004"
  # 5;"Shoprite";"SR";"S";"store0005"
  # 6;"Distribution Centre";"DC";"D";"store0006"
  # 7;"Hyperama";"HY";"H";"store0007"
  # 9;"Checkers Hyper";"CA";"A";"store0008"
  # 10;"OK Foods";"OF";"K";"store0009"
  # 11;"Non Foods";"NF";"N";"store0010"

  def self.update_stores

    `say Getting store codes`

    conn = connect_to_rr_db

    res = conn.exec("SELECT * FROM tblstore, tbldivision, tlkstore_type WHERE tblstore.lngsrregion = tbldivision.lngdiv AND ysnactive = '0' AND tblstore.lngstoretype = tlkstore_type.lngstoretype")

    store_codes = MmStoreCode.all
    res.each do |row|
      store_codes.where(lngstore: row['lngstore']).
          first_or_create(lngstore: row['lngstore'],
                          branch_code: row['strbranchcode'],
                          store_name: row['strstorename'],
                          store_type: row['lngstoretype'],
                          store_type_long: row['strstoretype'],
                          store_type_abbr: row['strstoretypeabbr'],
                          store_lang_region: row['lngsrregion'],
                          store_division: row['strdivname'],
                          halaal_store: false,
                          kosher_store: false,
                          include_afr: false,
                          exclude_store: false)
    end

    langs = conn.exec('SELECT lngstore, strlangcode, strlanguage FROM tblstore_langs, tlklanguage WHERE tblstore_langs.lnglanguage = tlklanguage.lnglanguage')

    langs.each do |lang|
      if lang['strlanguage'].include?('Afrikaans')
        store_codes.where(lngstore: lang['lngstore']).
            update(include_afr: true)
      end
    end

  rescue => e
    puts "Failed to connect to rrdbsql_sr or no dev/test rr_db was found! : #{e}"
    Rails.logger.fatal("Failed to connect to rrdbsql_sr or no dev/test rr_db was found! : #{e}")
  end

  private

  def self.connect_to_rr_db
    Rollbar.log_info("Getting Store codes")
      begin
      return  PG::Connection.new(host: '10.23.5.51',
                                 port: '5432',
                                 user: 'postgres',
                                 password: 'kantina',
                                 dbname: 'rrdbsql_sr')
      rescue => e
        Rollbar.log_error("Failed updating store codes while attempting to connect to: 10.23.5.51 - rrdbsql_sr :: #{e}")
        raise "Failed updating store codes while attempting to connect to: 10.23.5.51 - rrdbsql_sr :: #{e}"
      end
      puts 'Fetching store codes from dev/test rr_db...'
      MmStoreCodeSeeder.seed_table # for local dev purposes
  end
end
