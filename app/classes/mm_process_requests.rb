# frozen_string_literal: true
# Reads an excel file and parses each line to determine the requests. Requests are adverts that the client is wanting. each request is to be flighted twice per day for the the period requested in the relevant region. Where relevant the advert is to flight in Eng & Afr.
class MmProcessRequests
  attr_reader :client_excel, :import_id, :region_list, :region_names, :all_stores
  attr_reader :request_ok, :requests, :build, :region
  attr_accessor :store_region, :region_short, :store_lang_region, :store_name, :store_code

  def initialize(client_excel, import_id)
    # StoreCodeRequester.update_stores  # Fetch and update new stores from rr_db to the local stores table
    @client_excel = Roo::Spreadsheet.open(client_excel)
    @import_id = import_id
    @region_list = MmRegionList.all
    @region_names = @region_list.pluck(:region_long)
    @all_stores = MmStoreCode.all
    @brand_shorts = MmBrandShort.all
    runner
  end

  def runner
    current_region = nil
    request_ok = true
    test_data = []
    item_code = 0
    client_excel.each do |row|
      row.compact!
      row_type = ExcelRowTypes.set(row)

      next if row_type == 'ignore'

      region_new = region_list.where(region_long: region_name_fetch(row)) if row_type == 'region'
      if region_new != region
        region = region_new
        @region_short = region.first.region_short
        @store_lang_region = region.first.store_lang_region
        @request_ok = true
        @store_name = nil
        @store_code = nil
      end

      if row_type == 'store_specific'
        store_code = row[0].downcase.gsub('rr', '').to_i
        store = all_stores.where(branch_code: store_code).last
        @store_code = store_code
        if store.nil?
          @request_ok = false
          @store_region = 'error'
          @region_short = 'error'
          @store_lang_region = 'error'
          @store_name = 'error'
          @store_code = 'error'
        else
          @request_ok = true
          store_region = region_list.where(store_lang_region: store.store_lang_region).first
          @region_short = store_region.region_short
          @store_lang_region = store_region.store_lang_region
          @store_name = store.store_name
          @store_code = row[0].upcase
        end
      end

      if row_type == 'item_request'
        item_code = row[0]

      elsif row_type == 'multiple_item_requests'
        item_code = row[0].split('/').first.to_i
      end

      echo item_code
      if item_code > 0
        # @request_ok = false  if @store_lang_region.nil? || @region_short.nil?
         request = MmRequest.where('import_id = ? and item_code = ? and region_short = ?', import_id, item_code, @region_short).
            first_or_create(import_id: import_id,
                            item_code: item_code,
                            item_descr: description_cleanup(row),
                            brand_short: select_brand_short(row),
                            store_type: select_store_type(row),
                            region_short: @region_short,
                            store_lang_region: @store_lang_region,
                            store_code: @store_code,
                            store_name: @store_name,
                            from: row[3].to_s.strip,
                            to: row[4].to_s.strip,
                            price: price_extract(row[5]).to_s,
                            unit: row[6].delete('/').strip,
                            extra_1: row[7],
                            request_ok: @request_ok)
        request.save
      end
      item_code = 0
    end
  end

  def region_name_fetch(row)
    region_names.each do |region_name|
      return region_name if region_name.downcase.gsub(' ', '') == row.first.downcase.gsub(' ', '')
    end
  end

  def select_brand_short(row)
    res = @brand_shorts.where(brand: row[2].strip)
    return 'NOT FOUND!' if res.empty?
    res.first.brand_short
  end

  def select_store_type(row)
    res = @brand_shorts.where(brand: row[2].strip)
    return 'NOT FOUND!' if res.empty?
    res.first.store_type
  end


  def description_cleanup(row)
    row[1].strip.downcase.split.map(&:capitalize).join(' ')
  end

  def contains?(object, check_for)
    object.to_s.downcase.include?(check_for.downcase)
  end


  def store_specific?(object)
    object[0].to_s.downcase.include?('rr') && object[0].to_s.downcase.gsub('rr', '').to_i.is_a?(Integer)
  end

  def a_num?(str)
    !!Integer(str)
  rescue ArgumentError, TypeError
    false
  end

  def a_flighting?(row)
    if row[0].to_s.include?('/')
      return true if is_a_num?(row[0].split('/'))
    end
    is_a_num?(row)
  end

  def is_a_num?(row)
    row[0].is_a?(Float) || row[0].is_a?(Integer) || a_num?(row[0].to_s)
  end

  def base_name(attach)
    at = attach.attachment.file.file
    File.basename(at)
  end

  def price_extract(obj)
    obj = obj.downcase.delete('r')[/\d+/] if obj.is_a?(String)
    obj.to_f
  end
end
