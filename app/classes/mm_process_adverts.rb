# frozen_string_literal: true
class MmProcessAdverts

  def requests_to_adverts(requests)
    pre_rec_bodies = MmPreRecordedBodyFile.all
    adverts = MmAdvert.all
    region_list = MmRegionList.all

    # Iterate over the requests
    requests.each do |request|
      # Iterate over the languages
      build_language_list(region_list, request.store_lang_region).each do |lang|
        product_description = request.item_descr.downcase.titleize
        # next if product_description.nil?

        price = price_parser(request)
        unit = unit_type(request.unit)
        region = region_list.where(store_lang_region: request.store_lang_region).first
        store_description = %( #{request.store_code ||= ''}-#{request.store_name ||= ''})
        store_description = request.store_code.empty? || request.store_code.nil? ? '' : store_description
        lang_code = lang_code(lang)
        pb_available = check_for_pb(pre_rec_bodies, request.item_code, request.unit, lang_code)
        non_halaal = pre_rec_bodies.where(item_code: request.item_code).first.non_halaal if pb_available

        advert = adverts.where('import_id = ? and request_id = ? and lang_code = ?',
                               request.import_id, request.id, lang_code).
            first_or_create(import_id: request.import_id,
                            request_id: request.id,
                            description: %(#{product_description} #{price} per #{unit} #{request.region_short} #{request.brand_short}#{store_description} #{lang}),
                            product: product_description,
                            item_code: request.item_code,
                            brand_short: request.brand_short,
                            store_type: request.store_type,
                            region_short: request.region_short,
                            store_lang_region: request.store_lang_region,
                            store_specific: store_description.empty? ? false : true,
                            store_code: request.store_code ||= nil,
                            store_name: request.store_name ||= nil,
                            lang_full: lang_long(lang),
                            lang_code: lang_code,
                            theme: 'Standard',
                            theme_code: 'T0001',
                            currency: region.currency,
                            price: price,
                            price_code: generate_price_code(price),
                            unit_size: unit,
                            start_date: request.from.strftime('%d/%m/%Y'),
                            end_date: request.to.strftime('%d/%m/%Y'),
                            dsf: %(#{request.region_short} #{request.brand_short}#{store_description} #{lang}.DSF),
                            pb_available: pb_available,
                            non_halaal: non_halaal,
                            prev_spm_exists: nil,
                            prev_spm_advert_id: nil,
                            new_spm_generated: false,
                            spm_code: nil,
                            mp3_dsf_generated: false,
                            sns_dsf_generated: false,
                            sns_generated: false,
                            request_processed: false)
        advert.save
      end
    end
  end

  def price_parser(request)
    (request.price * 100).to_i.to_s.insert(-3, '.')
  end

  def build_language_list(region_list, store_lang_region)
    region_list.where(store_lang_region: store_lang_region).first.lang.downcase.titleize.split('&').map(&:strip)
  end

  # TODO: Make this more dynamic
  def unit_type(unit)
    return '1kg' if unit.downcase.include?('kg') || unit.downcase.include?('1000')
    return '100g' if unit.downcase.include?('100')
    return '150g' if unit.downcase.include?('150')
    return '200g' if unit.downcase.include?('200')
    return '250g' if unit.downcase.include?('2050')
    return '300g' if unit.downcase.include?('300')
    return '350g' if unit.downcase.include?('350')
    return '500g' if unit.downcase.include?('500')
    '********ERROR! Could not find a matching unit size!!!!********'
  end

  def brand_type(brand, product)
    ex_hal = true if product[3].downcase.include?('pork')
    type = MmBrandShort.where('lower(brand) = ?',
                              brand.downcase)[0].brand_short
    type = %(#{type} EX HAL) if ex_hal
    type
  rescue
    type = %(**ALERT!**: **Brand Short not found! [#{brand}]****)
    type = %(#{type} EX HAL) if ex_hal
    type
  end

  def region_select(region_list, region)
    region_list.where('lower(region_long) ILIKE ?', "%#{region}%")[0]
  end

  def lang_by_region(region_list, region)
    region_list.where('lower(region_long) ILIKE ?', "%#{region}%")[0].lang
  end

  def lang_long(lang)
    return 'English' if lang.downcase.include?('eng')
    return 'Afrikaans' if lang.downcase.include?('afr')
    '********ERROR! Language could not be determined!!!!********'
  end

  def lang_code(lang)
    return 'L02' if lang.downcase.include?('english')
    return 'L01' if lang.downcase.include?('afr')
    'L02'
  end

  # we first check if the PB exists.
  # then we check if the requested unit size exists.
  def check_for_pb(pre_rec_bodies, item_code, unit, lang_code)
    return false unless pre_rec_bodies.where('item_code = ? and lang_code = ?', item_code, lang_code).exists?
    return false if unit == pre_rec_bodies.where(item_code: 1_889_440)[0]
    true
  end

  def generate_price_code(price)
    res = '%0.2f' % price.to_f
    res = res.delete('.')
    "PR#{sprintf '%06d', res}"
  end

  def pbs_not_available(adverts)
    adverts.where(pb_available: false).to_a.uniq(&:description).size
  end

  def check_for_item(item_list, item)
    item_list.each do |arr|
      return arr if arr[0] == item.to_s
    end
    nil
  end
end
