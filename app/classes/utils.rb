
# frozen_string_literal: true
module Utils
  module_function

  def create_dirs
    FileUtils.mkdir_p ConstantsHelper::JOBS
    FileUtils.mkdir_p ConstantsHelper::LOGS
    FileUtils.mkdir_p ConstantsHelper::SPM_JSON
    FileUtils.mkdir_p ConstantsHelper::TEMP
    FileUtils.mkdir_p ConstantsHelper::MUSIC
    FileUtils.mkdir_p ConstantsHelper::PBS
    FileUtils.mkdir_p ConstantsHelper::VALS
    FileUtils.mkdir_p ConstantsHelper::TAGGING
    FileUtils.mkdir_p ConstantsHelper::OUTGOING
  end
end
