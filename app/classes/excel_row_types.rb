# Meat Market Excel Request sheet helper.
# Parses a row and determines the row type.

module ExcelRowTypes
  module_function

  STRING_TYPES = ['meat market',
                  'item no',
                  "specials",
                  'halaal',
                  'kosher']

  def set(row)
    test_report_output(row) # For Test Results Export
    row.compact!
    return 'ignore' if row.empty? || row.first.to_s.strip.empty?

    if row.first.is_a?(String)
      return 'ignore' if ignored_row_type?(row)
      store_type = check_store_types(row)
      return store_type if store_type
      region_type = check_region_types(row)
      return region_type if region_type
    end

    item_request_type = check_item_request(row) if row.size >= 7
    return item_request_type if item_request_type

    type_not_found(row)
  end

  def check_item_request(row)
    return 'item_request' if a_num?(row.first)
    return 'multiple_item_requests' if row.first.split('/').all? {|i| a_num?(i)}
  end

  def type_not_found(row)
    echo "Unknown Row Type: #{row}"
    # Rollbar.warning("Unknown Row Type: #{row}")
    'error_unknown'
  end

  def ignored_row_type?(row)
    STRING_TYPES.any? do |i|
      row.first.downcase.include?(i)
    end
  end

  def check_store_types(row)
    obj = row.first.downcase
    return unless obj.include?('rr')
    obj = obj.gsub('rr', '')
    return 'store_specific' if a_num?(obj) &! obj.include?('/')
    return "multi_store_specific" if obj.split('/').all? {|i| a_num?(i.to_i)}
    nil
  end

  def check_region_types(row)
    region_names.each do |region_name|
      return 'region' if region_name.downcase.gsub(' ', '') == row.first.downcase.gsub(' ', '')
    end
    nil
  end

  def region_names
    @region_names ||= MmRegionList.all.pluck(:region_long)
  end

  def a_num?(str)
    !!Integer(str)
  rescue ArgumentError, TypeError
    false
  end

  # For Test Results Export (The 'p' must stay)
  def test_report_output(row)
    puts row.inspect if Rails.env == 'test'
  end
end
