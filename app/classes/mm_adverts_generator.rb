# frozen_string_literal: true

class MmAdvertsGenerator
  include Utils
  include MmGenerateDistroFiles

  def self.run_process( job_id, import_id, current_user_id)
    ads = MmAdvert.where(import_id: import_id)

    spm_path = File.join(SPM, job_id.to_s)
    spm_json_path = File.join(SPM_JSON, job_id.to_s)
    spm_zip_file = "#{File.join(SPM, job_id.to_s)}.zip"
    Utils.create_dirs
    FileUtils.mkdir_p spm_path
    FileUtils.mkdir_p spm_json_path
    FileUtils.mkdir_p SPM_ZIP_PATH
    GenerateAdverts.run_process(ads, spm_path, import_id, current_user_id, spm_json_path)
    MmGenerateDistroFiles.run_process(ads, spm_path)

    ads.each do |ad|
      if ad.new_spm_generated && ad.mp3_dsf_generated && ad.sns_dsf_generated && ad.sns_generated
        ad.request_processed = true
      end
      ad.save!
    end

    WriteAdvertDetailsRrDb.run_process(ads)
    MoveFilesToDistroDir.run_process(ads)
    FileUtils.cp_r(spm_path + '/.', OUTGOING)
    Archiver.run(spm_path, spm_zip_file, import_id)

    Import.where(id: import_id).update(processed: true, note: %(Job completed: #{Time.now.localtime.strftime('%d %b %Y %H:%M:%S')}))
    MmRequestJob.where(import_id: import_id).update(job_completed: true, user_id: current_user_id)

  end
end

class GenerateAdverts
  include ActionView::Helpers::NumberHelper

  def self.run_process(ads, spm_path, _import_id, _current_user_id, spm_json_path)

    # Set a starting point for the new SPM's as not to clash with the current ones.
    spm_code = MmAdvert.pluck(:spm_code).compact.sort.last
    spm_code_num = spm_code.nil? ? 80_000 : spm_code.gsub('SPM', '').to_i

    voice_artists = VoiceArtist.all
    FileUtils.mkdir_p(spm_path)

    if Rails.env == 'production'
      `export NNAP_LOG_FILE=/home/devops/mm_factory/log/nnap.log`
      `export NNAP_TEMP_DIR=/home/devops/mm_factory/jobs/temp`
    end

    # Iterate over the list of advert to be created
    ads.sort.each do |ad|
      # reverse_merge
      advert = {}
      next if ad.prev_spm_exists || ad.new_spm_generated
      next unless ad.pb_available

      # The sPM code fro this advert
      spm_code = %(SPM#{format('%07d', spm_code_num += 1)})

      # Check if a previous SPM matches
      previous_matched_spm = MmAdvert.where('id != ?', ad.id).where(description: ad.description).first
      if previous_matched_spm && previous_matched_spm.mp3_dsf_generated.eql?(true)
        ad.prev_spm_exists = true
        ad.prev_spm_advert_id = previous_matched_spm.id
        ad.new_spm_generated = false
        spm_code = previous_matched_spm.spm_code
        ad.spm_code = spm_code
        prev_spm = Dir.glob("#{SPM}/**/*.mp3").select { |item| item.include?(spm_code) }.first
        FileUtils.cp(prev_spm, File.join(spm_path, spm_code + '.mp3'))

      else
        advert_json = compile_advert(ad, advert, spm_code, spm_json_path, spm_path, voice_artists)
        cmd = "#{RUBY} #{NNAP} #{advert_json}"
        puts %(Generating Advert: #{cmd})

        # `touch #{advert[:sp]}`
        `#{cmd}`
        ad.spm_code = spm_code
      end

      ad.new_spm_generated = File.exist?(File.join(spm_path, spm_code + '.mp3'))
      ad.save!

    end

  end

  def self.compile_advert(ad, advert, spm_code, spm_json_path, spm_path, voice_artists)
    lang_code = ad.lang_code
    pbs       = MmPreRecordedBodyFile.where('item_code = ? and lang_code = ?', ad.item_code, lang_code)
    pb        = nil

    pb = if pbs.size > 1 && ad.unit_size == 'kg'
           pbs.where(unit: 'kg')[0]
         else
           pbs.where(unit: 'dyn')[0]
         end

    pb = pbs[0] if pb.nil?

    price      = ad.price_code.gsub('PR', '').to_s
    value_a    = price[-5..-3]
    value_b    = price[-2..-1]
    voice_code = pb.rr_voice_code
    lang_code  = ad.lang_code

    val_a2 = ''
    val_b2 = ''
    val_a1 = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and value = ?',
                                    pb.rr_voice_code, lang_code, 'VALA1', value_a)[0].file_name
    val_a2 = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and value = ?',
                                    pb.rr_voice_code, lang_code, 'VALA2', value_a)[0]

    if value_b == '00'
      val_b1 = MmCurrency.where('curr_code = ? and rr_voice_code = ? and lang_code = ? and curr_type = ?',
                                ad.currency.upcase, voice_code, lang_code, 'CURRE1')[0].file_name

    else
      val_b1 = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and value = ?',
                                      pb.rr_voice_code, lang_code, 'VALB1', value_b)[0].file_name
      val_b2 = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and value = ?',
                                      pb.rr_voice_code, lang_code, 'VALB2', value_b)[0]
    end

    if pb.unit == 'dyn'
      unit_size = MmPriceValueFile.where('rr_voice_code = ? and lang_code = ? and value_type = ? and unit_size = ?', pb.rr_voice_code, lang_code, 'UNITSIZE', ad.unit_size)[0].file_name
      unit_size = File.join(VALS, unit_size)
    else
      unit_size = false
    end

    voice_artist = voice_artists.where('rr_voice_code = ? and lang_code =?', pb.rr_voice_code, lang_code).first
    music        = MusicFile.where(id: rand(MusicFile.all.size) + 1)[0].file_name

    advert[:sp]         = File.join(spm_path, spm_code + '.mp3')
    advert[:voice_code] = voice_code
    advert[:lang_code]  = lang_code
    advert[:pb]         = File.join(PBS, pb.file_name)
    advert[:unit_size]  = unit_size if pb.unit == 'dyn'
    advert[:val_a1]     = File.join(VALS, val_a1)
    advert[:val_b1]     = File.join(VALS, val_b1)
    advert[:val_a2]     = File.join(VALS, val_a2.file_name) unless val_a2.nil? || val_a2 == ''
    advert[:val_b2]     = File.join(VALS, val_b2.file_name) unless val_b2.nil? || val_b2 == ''
    advert[:curr]       = '' # Currently npt applicable on BertaApp ver 2.0
    advert[:pad1]       = voice_artist.pad_1.to_f
    advert[:pad2]       = voice_artist.pad_2.to_f
    advert[:pad3]       = voice_artist.pad_3.to_f
    advert[:curr_pad]   = voice_artist.curr_pad.to_f
    advert[:unit_pad]   = voice_artist.unit_pad.to_f
    advert[:music]      = File.join(MUSIC, music)
    advert[:script]     = ''

    advert_json = File.join(spm_json_path, spm_code + '.json')
    open(advert_json, 'w') { |f| f.puts JSON.pretty_generate(advert) }
    advert_json
  end
end

class Archiver
  def self.run(spm_path, spm_zip_file, import_id)

    cmd = %(zip -r -j #{spm_zip_file} #{spm_path})
    echo "Archive the job: #{cmd}"
    begin
      `#{cmd}`
    rescue => e
      puts "Failed to create zip file: #{e}"
    end
    if File.exist?(spm_zip_file)
      MmRequestJob.where(import_id: import_id).update(job_zip_created: true)
      puts "Zip file created!"
    else
      puts "Failed creating zip file!"
    end
  end
end

class WriteAdvertDetailsRrDb
  def self.run_process(_ads)
    echo 'WriteAdvertDetailsRrDb'
  end
end

class MoveFilesToDistroDir
  def self.run_process(_ads)

  end
end
