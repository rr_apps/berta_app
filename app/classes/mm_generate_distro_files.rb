
# frozen_string_literal: true
# Generate the DSF file used for the distribution of the files to stores
# 5SPM0061812.mp3.dsf
module MmGenerateDistroFiles
  module_function

  def run_process(ads, spm_path)
    store_codes = MmStoreCode.all
    priority = '5'
    ads.each do |ad|

      # Skip if the SPM MP3 has not been generated.
      next unless ad.new_spm_generated

      ad_spm_code   = ad.spm_code
      ad_start_date = ad.start_date
      ad_end_date   = ad.end_date
      rr_dates      = date_range_to_rr_date(ad_start_date, ad_end_date)
      time_now      = Time.now.strftime('%H%M%S')
      branch_codes, store_details = build_store_codes(ad, store_codes)

      # 5SPM0074470.mp3.dsf
      # 5        | SPM0074470|.mp3.dsf
      # priority | MP3 SPM # | file ext
      ## RR000301
      ## RR000521
      unless ad.mp3_dsf_generated
        mp3_dsf_file_name = %(5#{ad.spm_code}.mp3.dsf)
        File.atomic_write(File.join(spm_path, mp3_dsf_file_name)) do |file|
          file.write(branch_codes)
        end

        # mp3_dsf_file_name = %(5#{ad.spm_code}-#{ad.description}.mp3.dsf)
        # File.atomic_write(File.join(spm_path, mp3_dsf_file_name)) do |file|
        #   file.write(store_details)
        # end

        ad.mp3_dsf_generated = true
      end

      # 56780150539SPM0074470.sns.dsf
      # 5        | 6780    |  150539  | SPM0074470|.sns.dsf
      # priority | rr_date | HH:MM:SS | MP3 SPM # | file ext
      ## RR000301
      ## RR000521
      unless ad.sns_dsf_generated
        sns_dsf_file_name = %(5#{rr_dates[0]}#{time_now}#{ad_spm_code}.sns.dsf)
        File.atomic_write(File.join(spm_path, sns_dsf_file_name)) do |file|
          file.write(branch_codes)
        end

        # sns_dsf_file_name = %(5#{rr_dates[0]}#{time_now}#{ad_spm_code}-#{ad.description}.sns.dsf)
        # File.atomic_write(File.join(spm_path, sns_dsf_file_name)) do |file|
        #   file.write(store_details)
        # end
        ad.sns_dsf_generated = true
      end

      # 6780150539SPM0074470.sns
      # 6780    |  150539  | SPM0074470|.sns
      # rr_date | HH:MM:SS | MP3 SPM # | file ext
      # 67860299991100SPM0074470.snssns
      # 67850299991100SPM0074470.sns
      # 67840299991100SPM0074470.sns
      # 67830299991100SPM0074470.sns
      # 67820299991100SPM0074470.sns
      # 67810299991100SPM0074470.sns
      # 67800299991100SPM0074470.sns
      # 70880299991600SPM0076752.sns
      unless ad.sns_generated

        sns_file_name = %(#{rr_dates[0]}#{time_now}#{ad_spm_code}.sns)
        priority    = '02'
        ignore      = '9999'
        time_slots  = [%w(13 18), %w(12 17), %w(07 16), %w(09 14), %w(08 11), %w(10 15), %w(09 14)].shuffle
        schedule    = ''
        date_range  = range_of_rr_dates(ad_start_date, ad_end_date)
        date_range[0..6].each do |day|
          res = date_range.find_index(day)

          schedule += %(#{day}#{priority}#{ignore}#{time_slots[res][0]}00#{ad_spm_code}.mp3\n)
          schedule += %(#{day}#{priority}#{ignore}#{time_slots[res][1]}00#{ad_spm_code}.mp3\n)
        end

        File.atomic_write(File.join(spm_path, sns_file_name)) do |file|
          file.write(schedule)
        end
        ad.sns_generated = true
      end
      ad.save!
    end
  end

  def self.build_store_codes(ad, store_codes)
    if ad.store_specific
      code = store_codes.where(branch_code: ad.store_code.downcase.delete('rr').to_i).last
      return [rr_store_build(code.branch_code), build_details(code)]
    end

    codes = branch_codes(ad, store_codes)

    branch_codes = ''
    store_details = ''
    codes.each do |code|
      next if code.exclude_store
      next if non_afrikaans_store(ad, code)
      next if code.halaal_store && ad.non_halaal
      next if code.kosher_store && ad.non_halaal
      branch_codes += rr_store_build(code.branch_code) + "\n"
      store_details += build_details(code)
    end
    [branch_codes, store_details]
  end

  def self.build_details(code)
    %(#{rr_store_build(code.branch_code)}\t-\t#{code.store_name}\t-\t#{code.store_division}\t-\t#{code.store_lang_region}\t-\t#{code.store_type_long}\t-\tinclude_afr: #{code.include_afr}\t-\thalaal: #{code.halaal_store}\n)
  end

  def self.branch_codes(ad, store_codes)
    store_lang_region = ad.store_lang_region
    codes = []

    ad.store_type.split(',').each do |type|
      type = type.to_i
      if ad.region_short.eql?('GN') && (type.eql?(2) || type.eql?(5))
        codes += store_codes.where(store_type: type).where(store_lang_region: 6)
        codes += store_codes.where(store_type: type).where(store_lang_region: 4)
      elsif ad.region_short.eql?('GN') && (type.eql?(1) || type.eql?(9))
        codes += store_codes.where(store_type: type).where(store_lang_region: 20)
      else
        codes += store_codes.where(store_type: type).where(store_lang_region: store_lang_region)
      end
    end
    codes
  end

  def self.non_afrikaans_store(ad, code)
    ad.lang_code.eql?('L01') && !code.include_afr.eql?(true)
  end

  def rr_store_build(val)
    %(RR#{'%0.6d' % val})
  end

  # RR Dates are a 4-digit code representing how many days have passed since 1998-01-01.
  # ie, RR date 2867 means 2005-11-07
  # SNS files (in their current format) will start having serious problems in 2025, when the RR date reaches 9999.
  def self.rr_date(date)
    ((Time.strptime(date, '%d/%m/%Y') - Time.new(1998, 0o1, 0o1)) / 86_400).to_i
  end

  def self. date_range_to_rr_date(start_date, end_date)
    dates = []
    (Date.parse(start_date.to_s)..Date.parse(end_date.to_s)).each do |date|
      rr_date = ((Time.strptime(date.to_s, '%Y-%m-%d') - Time.new(1998, 0o1, 0o1)) / 86_400).to_i
      dates << rr_date
    end
    dates
  end

  def generate_rr_code(code)
    "RR#{sprintf '%06d', code}"
  end

  def self.range_of_rr_dates(date_start, date_end)
    arr = []
    (Date.parse(date_start.to_s)..Date.parse(date_end.to_s)).to_a.each do |date|
      arr << ((Time.parse(date.to_s) - Time.new(1998, 0o1, 0o1)) / 86_400).to_i
    end
    arr
  end
end
