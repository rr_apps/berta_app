# frozen_string_literal: true
ActiveAdmin.register VoiceArtist do
  actions :all, except: [:destroy]
  config.per_page = 400
end
