# frozen_string_literal: true
ActiveAdmin.register MmStoreCode do
  actions :all, except: [:destroy]
  config.per_page = 400
  config.batch_actions = true
  config.paginate = false
  config.sort_order = 'store_division'
end
