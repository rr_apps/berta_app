# frozen_string_literal: true
ActiveAdmin.register MmAdvert do
  actions :all, except: [:destroy, :edit]
  config.batch_actions = true
  config.per_page = 400
end
