# frozen_string_literal: true
ActiveAdmin.register MusicFile do
  actions :all, except: [:destroy, :edit]
  config.per_page = 400
end
