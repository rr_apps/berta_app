# frozen_string_literal: true
class MmStoreCode < ApplicationRecord

  scope :has_halaal_stores, -> { where(halaal_store: true) }

end
