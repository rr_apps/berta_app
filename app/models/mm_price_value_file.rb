# frozen_string_literal: true
class MmPriceValueFile < ActiveRecord::Base

  def self.voice_artist_values(_actor_name = nil)
    Actor.joins('INNER JOIN voice_artists ON voice_artists.rr_voice_cose = price_value_files.rr_voice_cose').where(rr_voice_code: rr_voice_code)
  end
end
