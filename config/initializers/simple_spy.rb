# frozen_string_literal: true
module SimpleSpy
  def spy(msg, len = 35)
    print '-' * len + "\n" + caller[0].split('/')[-1] + "\n" + msg.class.to_s
    print ' : ' + msg.to_s + "\n" + '-' * len + "\n" * 2
  end
end

include SimpleSpy
