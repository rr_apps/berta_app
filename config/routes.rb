# frozen_string_literal: true
Rails.application.routes.draw do
  # get 'mm_request/index'
  #
  # get 'mm_request/show'
  #
  # get 'mm_process_excel_import/index'
  #
  # get 'mm_process_excel_import/show'
  #
  # get 'long_services/index'
  #
  # get 'long_services/jobs'
  #
  # get 'long_services/assets'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  #
  # get 'generators/index'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  get 'mm_assets/pb_bodies'

  get 'mm_assets/music'

  get 'mm_assets/price_vals'

  get 'mm_assets/index'

  get 'mm_assets/show'

  get 'mm_assets/required'

  #
  get 'mm_reports/halaal_stores'

  get 'mm_reports/non_halaal_products'


  resources :berta, only: [:index] do
    get :index, on: :member
  end


get 'imports/destroy_all'
  resources :imports, only: [:index, :new, :create, :destroy] do
    put :process_zip_file, on: :member
    put :process_mp3_file, on: :member
    # get :destroy_all, on: :member
  end

  # get 'mm_requests/new_requests'

  resources :mm_adverts

  resources :mm_requests

  get 'mm_request_jobs/download_job'

  resources :mm_request_jobs

  resources :ls_requests

  root 'berta#index'

end
