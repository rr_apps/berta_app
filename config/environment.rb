# frozen_string_literal: true
# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Set the default host and port to be the same as Action Mailer.
# BertaApp::Application.default_url_options = BertaApp::Application.config.action_mailer.default_url_options

# Initialize the Rails application.
Rails.application.initialize!
