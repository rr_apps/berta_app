require 'test_helper'

class LongServicesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get long_services_index_url
    assert_response :success
  end

  test "should get jobs" do
    get long_services_jobs_url
    assert_response :success
  end

  test "should get assets" do
    get long_services_assets_url
    assert_response :success
  end

end
