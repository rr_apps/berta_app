require 'test_helper'

class MmRequestControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get mm_request_index_url
    assert_response :success
  end

  test "should get show" do
    get mm_request_show_url
    assert_response :success
  end

end
