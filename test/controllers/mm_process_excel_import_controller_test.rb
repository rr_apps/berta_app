require 'test_helper'

class MmProcessExcelImportControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get mm_process_excel_import_index_url
    assert_response :success
  end

  test "should get show" do
    get mm_process_excel_import_show_url
    assert_response :success
  end

end
